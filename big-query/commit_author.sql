SELECT
  repo.repo_name as repo_name,
  commits.author.name as author_name,
  commits.author.email as author_email,
  commits.author.date as commit_data
FROM (
  SELECT
    *
  FROM
    [bigquery-public-data:github_repos.sample_repos]
  WHERE
    watch_count > 10 ) AS repo
inner join [bigquery-public-data:github_repos.sample_commits] as commits
on repo.repo_name = commits.repo_name
