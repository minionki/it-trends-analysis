SELECT
  repo.repo_name AS repo_name,
  lang.language.name AS lang_name,
  lang.language.bytes AS lang_bytes,
  repo.watch_count AS watch_count
FROM (
  SELECT
    *
  FROM
    [bigquery-public-data:github_repos.sample_repos]
  WHERE
    watch_count > 10 ) AS repo
INNER JOIN
  [bigquery-public-data:github_repos.languages] AS lang
ON
  repo.repo_name = lang.repo_name
WHERE
  lang.language.name IS NOT NULL
  