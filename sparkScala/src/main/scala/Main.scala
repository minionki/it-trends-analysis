import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.neo4j.spark.Neo4j

object Main {
  def main(args: Array[String]) {

    val sparkConf = new SparkConf()
      .set("spark.neo4j.bolt.password", "password")
      .set("spark.neo4j.bolt.url", "bolt://10.100.13.105:7687")

    val spark = SparkSession.builder
      .appName("Graph test")
      .master("local[*]")
      .config(sparkConf)
      .getOrCreate

    System.out.println("CREATE SPARK CONTEXT")

    val neo4j = new Neo4j(spark.sparkContext)

    System.out.println("LOAD GRAPH")

    val graphFrame = neo4j.pattern(("Tag", "tag"), ("Connected", "count"), ("Tag", "tag")).loadGraphFrame

    System.out.println("GRAPH LOADED: " + graphFrame.vertices.count())

    val pageRankFrame = graphFrame.pageRank.maxIter(50).run()
    val ranked = pageRankFrame.vertices

    System.out.println("SIZE: " + ranked.count())
    val rOrderd = ranked.orderBy(ranked.col("pagerank").desc).take(100)
    rOrderd.show(20)

    spark.stop()
  }
}
