name := "sparkScala"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.5"  % "provided"
libraryDependencies += "org.apache.spark" %% "spark-graphx" % "2.4.5"  % "provided"

resolvers += "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven"
libraryDependencies += "neo4j-contrib" % "neo4j-spark-connector" % "2.4.1-M1"