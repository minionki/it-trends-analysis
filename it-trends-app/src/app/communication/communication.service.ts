import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment.prod";
import {Observable} from "rxjs";
import { AllTagsResponse } from "../tag-classes/tag-info"
import { HistoryRequest, HistoryResponse } from '../tag-classes/tag-history'
import {TechnologySuggestRequest, TechnologySuggestResponse} from "../tag-classes/tech-suggest";

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private serverPath = environment.serverPath;

  constructor(private httpClient: HttpClient) { }

  getTags(): Observable<AllTagsResponse> {
    return this.httpClient.get<AllTagsResponse>(this.serverPath + '/tags');
  }

  getHistory(historyRequest: HistoryRequest): Observable<HistoryResponse> {
    return this.httpClient.post<HistoryResponse>(this.serverPath + '/history', historyRequest);
  }

  getConnectedTechnology(technologyRequest: TechnologySuggestRequest): Observable<TechnologySuggestResponse> {
    return this.httpClient.post<TechnologySuggestRequest>(this.serverPath + '/technology', technologyRequest);
  }
}
