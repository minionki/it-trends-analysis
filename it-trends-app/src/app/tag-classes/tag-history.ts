
export class HistoryResponse {
  history: Array<TagHistory>;
}

export class TagHistory {
  tag: string;
  popularity: Array<DayInfo>;
}

export class DayInfo{
  date: string;
  count: number;
}

export class HistoryRequest {
  tags: Array<String>;
//( pattern = "dd.MM.yyyy")
  since: string;
//( pattern = "dd.MM.yyyy")
  to: string;

  constructor(tags, since, to) {
    this.tags = tags;
    this.to = to;
    this.since = since;
  }
}
