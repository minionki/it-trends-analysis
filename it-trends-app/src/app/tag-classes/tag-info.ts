import {Tag} from "./tag";

export class TagInfo {
  tags: Array<Tag>;
}

export class AllTagsResponse{
  tags: Array<string>
}
