export class TechnologySuggestResponse {
  tags: Array<string>;
}

export class TechnologySuggestRequest {
  tags: Array<string>;

  constructor(tags: Array<string>) {
    this.tags = tags;
  }
}
