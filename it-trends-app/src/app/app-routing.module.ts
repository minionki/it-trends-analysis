import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainPageComponent} from "./main-page/main-page.component";
import {TechnologyListPageComponent} from "./technology-list-page/technology-list-page.component";
import {HistoryPageComponent} from "./history-page/history-page.component";


const routes: Routes = [
  {path: '', component: MainPageComponent},
  {path: 'home', component: MainPageComponent},
  {path: 'connected-technologies', component: TechnologyListPageComponent},
  {path: 'popularity-history', component: HistoryPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
