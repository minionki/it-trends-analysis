import { TestBed } from '@angular/core/testing';

import { ColChartService } from './col-chart.service';

describe('ColChartService', () => {
  let service: ColChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
