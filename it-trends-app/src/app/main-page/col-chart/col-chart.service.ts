import { Injectable } from '@angular/core';
import {Tag} from "../../tag-classes/tag";
import {TagInfo} from "../../tag-classes/tag-info";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ColChartService {
  private tagsData:  Array<Tag> = [];
  private update = new Subject<Array<Tag>>();

  constructor() { }

  updateTagData(data: TagInfo): void {
    this.tagsData = data.tags;
    this.update.next(this.getMaxData());
  }

  private getMaxData() {
    let data = this.tagsData.sort((a, b) => b.count - a.count).slice(0, 10);
    return data;
  }

  getData(): Observable<Array<Tag>> {
    return this.update.asObservable();
  }
}
