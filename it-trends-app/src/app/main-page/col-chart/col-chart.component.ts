import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../canvasjs.min';
import {environment} from "../../../environments/environment";
import {ColChartService} from "./col-chart.service";
import {Subscription} from "rxjs";
import {Tag} from "../../tag-classes/tag";

@Component({
  selector: 'app-col-chart',
  templateUrl: './col-chart.component.html',
  styleUrls: ['./col-chart.component.css']
})
export class ColChartComponent implements OnInit {

  private chart : CanvasJS.Chart;
  private subscription: Subscription;

  constructor(private colChartService: ColChartService) { }

  ngOnInit() {
    this.chart = new CanvasJS.Chart("colChartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      backgroundColor: null,
      title: {
        text: "Technology popularity",
        fontFamily: environment.text,
        fontWeight: "bold"
      },
      axisX:{
        labelFontFamily: environment.text
      },
      axisY:{
        labelFontFamily: environment.text
      },
      data: [{
        type: "column"
      }]
    });

    this.chart.render();

    this.subscription = this.colChartService.getData().subscribe( data => {
      this.updateChart(data);
    })
  }

  private updateChart(data: Array<Tag>) : void {
    this.chart.options.data[0].dataPoints = [];
    for (let tag of data) {
      this.chart.options.data[0].dataPoints.push({y : tag.count, label : tag.name});
    }
    this.chart.render();
  }
}
