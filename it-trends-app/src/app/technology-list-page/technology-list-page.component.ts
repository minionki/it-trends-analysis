import { Component, OnInit } from '@angular/core';
import {CommunicationService} from "../communication/communication.service";
import {Subscription} from "rxjs";
import {SelectService} from "../select-components/select.service";
import {TechnologySuggestRequest, TechnologySuggestResponse} from "../tag-classes/tech-suggest";
import {environment} from "../../environments/environment.prod";

@Component({
  selector: 'app-technology-list-page',
  templateUrl: './technology-list-page.component.html',
  styleUrls: ['./technology-list-page.component.css']
})
export class TechnologyListPageComponent implements OnInit {

  suggests: boolean = false;
  progressbarif: boolean = false;
  suggestedTechnologies: Array<string>;
  private subscription: Subscription;

  constructor(private communicationService : CommunicationService,
              private selectService: SelectService) {
    this.subscription = this.selectService.getSelectedTech().subscribe((data: Array<string>) => {
      if (data.length > 0) {
        this.progressbarif = true;
        this.suggests = false;
        let techRequest = new TechnologySuggestRequest(data);
        this.communicationService.getConnectedTechnology(techRequest).subscribe((data: TechnologySuggestResponse) => {
          this.suggestedTechnologies = data.tags;
          this.suggests = !(!this.suggestedTechnologies || this.suggestedTechnologies.length == 0);
          this.progressbarif = false;
        })
      }
      else {
        this.suggests = false;
        this.progressbarif = false;
      }
    })
  }

  ngOnInit(): void {
    let techRequest = new TechnologySuggestRequest(environment.defaultTags);
    this.progressbarif = true;
    this.communicationService.getConnectedTechnology(techRequest).subscribe((data: TechnologySuggestResponse) => {
      this.suggestedTechnologies = data.tags;
      this.suggests = !(!this.suggestedTechnologies || this.suggestedTechnologies.length == 0);
      this.progressbarif = false;
    })
  }

}
