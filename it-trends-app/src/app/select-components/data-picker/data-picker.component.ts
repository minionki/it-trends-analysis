import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms";
import {SelectService} from "../select.service";

@Component({
  selector: 'app-data-picker',
  templateUrl: './data-picker.component.html',
  styleUrls: ['./data-picker.component.css']
})
export class DataPickerComponent implements OnInit {

  startDate = new FormControl();
  endDate = new FormControl();
  max_date: Date;
  to_date: Date;
  since_date: Date;

  constructor(private selectService: SelectService) { }

  ngOnInit(): void {
    this.max_date = new Date();
    this.to_date = new Date();
    this.since_date = new Date(this.to_date.getFullYear(), this.to_date.getMonth() - 1, this.to_date.getDay());
    this.startDate.setValue(this.since_date);
    this.endDate.setValue(this.to_date);
    this.selectService.updateStartDate(this.since_date);
    this.selectService.updateEndDate(this.to_date);
  }

  onStartDataChange(event): void {
    if (event.value != null)
      this.since_date = event.value;
    else {
      this.since_date = new Date(this.to_date.getFullYear(), this.to_date.getMonth() - 1, this.to_date.getDay());
      this.startDate.setValue(this.since_date);
    }

    this.selectService.updateStartDate(this.since_date);
  }

  onEndDataChange(event): void {
    if (event.value != null)
      this.to_date = event.value;
    else {
      this.to_date = new Date();
      this.endDate.setValue(this.to_date);
    }

    this.selectService.updateEndDate(this.to_date);
  }
}
