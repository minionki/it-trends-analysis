import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SelectService {
  private selected_technologies = new Subject<Array<string>>();
  private since_date = new Subject<Date>();
  private to_date = new Subject<Date>();

  constructor() { }

  updateSelectedTech(data: Array<string>): void {
    this.selected_technologies.next(data);
  }

  getSelectedTech(): Observable<Array<string>> {
    return this.selected_technologies.asObservable();
  }

  updateStartDate(data: Date): void {
    this.since_date.next(data);
  }

  getStartDate(): Observable<Date> {
    return this.since_date.asObservable();
  }

  updateEndDate(data: Date): void {
    this.to_date.next(data);
  }

  getEndDate(): Observable<Date> {
    return this.to_date.asObservable();
  }
}
