import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable} from "rxjs";

import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {FormControl} from "@angular/forms";
import {MatChipInputEvent} from "@angular/material/chips";
import {MatAutocomplete, MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {map} from "rxjs/operators";
import {environment} from "../../../environments/environment.prod";
import {SelectService} from "../select.service";

@Component({
  selector: 'app-tech-select',
  templateUrl: './tech-select.component.html',
  styleUrls: ['./tech-select.component.css']
})

export class TechSelectComponent implements OnInit {

  selected_technologies : Array<string>;
  selectable = false;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  techCtrl = new FormControl();
  filteredTech: Observable<string[]>;

  @ViewChild('techInput') techInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private selectService : SelectService) {  }

  ngOnInit(): void {
    this.filteredTech = this.techCtrl.valueChanges.pipe(
      map((tech: string | null) => tech && tech.length >= 3 ?
        environment.technologies.filter(v => v.toLowerCase().indexOf(tech.toLowerCase()) === 0) :
        []));
    //environment.technologies.slice()
    this.selected_technologies = environment.defaultTags;

    this.selectService.updateSelectedTech(this.selected_technologies);
  }

  onSelected(event: MatAutocompleteSelectedEvent): void {
    if(!this.selected_technologies.includes(event.option.viewValue)) {
      this.selected_technologies.push(event.option.viewValue);
    }
    this.techInput.nativeElement.value = '';
    this.techCtrl.setValue(null);
    this.selectService.updateSelectedTech(this.selected_technologies);
  }

  onRemove(tech: string): void {
    let i = this.selected_technologies.indexOf(tech);
    this.selected_technologies.splice(i, 1);
    this.selectService.updateSelectedTech(this.selected_technologies);
  }

  onAdd(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      let tech = value.trim();
      tech = tech.toLowerCase();
      if (environment.technologies.includes(tech) && !this.selected_technologies.includes(tech)) {
        this.selected_technologies.push(tech);
        this.selectService.updateSelectedTech(this.selected_technologies);
      }
    }

    if (input) {
      input.value = '';
    }

    this.techCtrl.setValue(null);
  }

}
