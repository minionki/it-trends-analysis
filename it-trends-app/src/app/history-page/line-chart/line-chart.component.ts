import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../canvasjs.min';
import {environment} from "../../../environments/environment";
import {LineChartService} from "./line-chart.service";
import {Subscription} from "rxjs";
import {TagHistory} from "../../tag-classes/tag-history";
import {Tag} from "../../tag-classes/tag";


@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  private chart : CanvasJS.Chart;
  private subscription: Subscription;

  constructor(private lineChartService: LineChartService) { }

  ngOnInit(): void {
    this.chart = new CanvasJS.Chart("lineChartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      backgroundColor: null,
      title: {
        text: "Technology popularity in time",
        fontFamily: environment.text,
        fontWeight: "bold"
      },
      axisX:{
        labelFontFamily: environment.text
      },
      axisY:{
        labelFontFamily: environment.text
      },
      toolTip: {
        shared: true
      },
      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: false,
        itemclick: this.toogleDataSeries
      },
      data: []
    });
    this.chart.render();

    this.subscription = this.lineChartService.getTagsHistory().subscribe((tagsHistory: Array<TagHistory>) => {
      this.updateChart(tagsHistory);
    })
  }

  private toogleDataSeries(e){
    e.dataSeries.visible = !(typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible);
    e.chart.render();
  }

  private updateChart(tagsHistory: Array<TagHistory>) : void {
    this.chart.options.data = [];
    let i = 0;
    for (let history of tagsHistory) {
      this.chart.options.data.push({type: 'line', label: history.tag, name: history.tag, showInLegend: true, xValueType: "dateTime", dataPoints: []});
      for (let day of history.popularity) {
        this.chart.options.data[i].dataPoints.push({x: Date.parse(day.date), y : day.count});
      }
      i = i + 1;
    }
    //console.log(this.chart.options)
    this.chart.render();
  }
}
