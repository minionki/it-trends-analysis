import { Injectable } from '@angular/core';
import {Observable, Subject, Subscription} from "rxjs";
import {HistoryRequest, HistoryResponse, TagHistory} from "../../tag-classes/tag-history";
import {SelectService} from "../../select-components/select.service";
import {CommunicationService} from "../../communication/communication.service";
import {DatePipe} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class LineChartService {
  private tagsHistory = new Subject<Array<TagHistory>>();

  private startDataSubscription: Subscription;
  private endDataSubscription: Subscription;
  private techSubscription: Subscription;

  private since_date: Date;
  private to_date: Date;
  private selected_technologies: Array<string> = [];

  constructor(private selectService: SelectService,
              private communicationService: CommunicationService,
              private datePipe: DatePipe) {

    this.startDataSubscription = this.selectService.getStartDate().subscribe((start_date: Date) => {
      this.since_date = start_date;
      this.getHistory();
    });

    this.endDataSubscription = this.selectService.getEndDate().subscribe((end_date: Date) => {
      this.to_date = end_date;
      this.getHistory();
    });

    this.techSubscription = this.selectService.getSelectedTech().subscribe((selected_tech: Array<string>) => {
      this.selected_technologies = selected_tech;
      this.getHistory();
    })
  }

  getTagsHistory(): Observable<Array<TagHistory>> {
    return this.tagsHistory.asObservable();
  }

  private getHistory(): void {
    if(this.since_date != null && this.to_date != null && this.selected_technologies != null) {
      let historyRequest = new HistoryRequest(this.selected_technologies,
                                              this.datePipe.transform(this.since_date, 'dd.MM.yyyy'),
                                              this.datePipe.transform(this.to_date, 'dd.MM.yyyy'));

      this.communicationService.getHistory(historyRequest).subscribe((historyResponse: HistoryResponse) => {
        this.tagsHistory.next(historyResponse.history);
      });
    }
  }
}
