import {Component, OnInit} from '@angular/core';
import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import {AllTagsResponse, TagInfo} from "./tag-classes/tag-info";
import {ColChartService} from "./main-page/col-chart/col-chart.service";
import {CommunicationService} from "./communication/communication.service";
import {environment} from "../environments/environment.prod";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'IT Trends Tracker';
  stompService: StompService;

  private stompConfig: StompConfig = {
    url: 'ws://35.240.2.255:30036/tag',
    headers: {
      login: '',
      passcode: ''
    },
    heartbeat_in: 0,
    heartbeat_out: 20000,
    reconnect_delay: 5000,
    debug: true
  };

  constructor(private colChartService: ColChartService,
              private communicationService: CommunicationService,
              public router: Router) {}

  ngOnInit(): void {
    this.communicationService.getTags().subscribe((allTags: AllTagsResponse) => {
      environment.technologies = allTags.tags;
      console.log(environment.technologies);
    });
    this.stompService = new StompService(this.stompConfig);
    this.stompService.subscribe('/topic/messages').subscribe(value => {
      let body: TagInfo = JSON.parse(value.body);
      this.colChartService.updateTagData(body);
    });
  }
}
