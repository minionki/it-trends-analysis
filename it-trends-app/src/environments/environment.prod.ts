export const environment = {
  production: true,
  text: "Lucida Console",
  serverPath: "http://35.240.2.255:30036",
  technologies: [
    'c#',
    'python',
    'c++',
    'java'
  ],
  defaultTags: [
    'java',
    'javascript',
    'spring'
  ]
};
