# Environment setup

## 1. Cluster initialization

**TODO description**

## 2. OpenVPN

Based on the following [article](https://itnext.io/use-helm-to-deploy-openvpn-in-kubernetes-to-access-pods-and-services-217dec344f13).

### 2.1. Create StorageClass

```
kubectl create -f slow-storage-class.yaml
```

### 2.2. Create PersistentVolumeClaim

```
kubectl create -f openvpn-pvc.yaml
``` 

### 2.3. Configure Helm

```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
```

### 2.4. Create OpenVPN

```
helm install --name openvpn -f openvpn.yaml stable/openvpn
```

### 2.5. Create NodePort

```
kubectl create -f openvpn-nodeport.yaml
```

### 2.6. Network settings

#### 2.6.1 External IP

Menu > Networking > VPC network > External IP addresses

Change external address type from ephemeral to static and name it meaningfully, e.g. **big-data-ip**

#### 2.6.2 Firewall rule

Menu > Networking > VPC network > Firewall rules

Create new rule 

Targets: All instances in network
Source IP ranges: 0.0.0.0/0
Protocols and ports: tcp: 30350 (must be the same as in nodeport configuration)

### 2.7. Connect to VPN

#### 2.7.1 Create client cert required for connection 

* IP = **big-data-ip** (static, external IP address)
* POD_NAME = *see command below*
* KEY_NAME = whatever you want

Command for finding openvpn pod name

```
kubectl get pods | grep openvpn
```

Create new cert
```
kubectl exec -it POD_NAME /etc/openvpn/setup/newClientCert.sh KEY_NAME IP
```

Copy created cert from pod to local environment
```
kubectl exec -it POD_NAME cat /etc/openvpn/certs/pki/KEY_NAME.ovpn > KEY_NAME.ovpn
```

Download file from cloud
```
cloudshell download KEY_NAME.ovpn
```

Open KEY_NAME.ovpn and change port

- [- remote IP 443 tcp -]
- {+ remote IP PORT tcp +}


#### 2.7.2 Install OpenVPN client 

[Installation link for windows](https://openvpn.net/client-connect-vpn-for-windows/)

Set previously created KEY_NAME.ovpn as configuration file and connect.

In case of troubles, firstly check if

* KEY_NAME.ovpn file has correct IP and port (35.240.2.255 30350 in our case)
* your host/router has no firewall rules blocking connection to desired port

After successfull connection you will be able to access addresses from kubernetes private network