package ittrendsserver.ittrendsserver

import org.springframework.context.annotation.Configuration
import org.springframework.messaging.handler.annotation.MessageExceptionHandler
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.messaging.simp.annotation.SubscribeMapping
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.stereotype.Controller
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import java.text.SimpleDateFormat


data class TagInfo(
    val tags: List<Tag>
);

@Configuration
@EnableWebSocketMessageBroker
class WebSocketConfig : WebSocketMessageBrokerConfigurer {
    override fun configureMessageBroker(config: MessageBrokerRegistry) {
        config.enableSimpleBroker("/topic")
    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/tag").setAllowedOrigins("*")
    }
}

@Controller
class WebscoketController(
    private val messagingTemplate: SimpMessageSendingOperations
) {

    fun send(tags: List<Tag>) {
        messagingTemplate.convertAndSend("/topic/messages",TagInfo(tags));
    }

}
