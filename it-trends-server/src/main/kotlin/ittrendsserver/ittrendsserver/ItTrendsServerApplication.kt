package ittrendsserver.ittrendsserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ItTrendsServerApplication

fun main(args: Array<String>) {
    runApplication<ItTrendsServerApplication>(*args)
}
