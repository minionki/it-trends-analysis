package ittrendsserver.ittrendsserver

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

data class Tag(val name: String, val count: Int)

data class TagUpdateRequest(val tags: List<Tag>)

@RestController
class TagUpdateController(
    val tagService: TagService
) {

    @PostMapping("/update")
    fun updateTag(@RequestBody request: TagUpdateRequest) {
        tagService.updateTag(request.tags)
    }
}
