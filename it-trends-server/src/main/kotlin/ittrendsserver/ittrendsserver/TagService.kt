package ittrendsserver.ittrendsserver

import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class TagService(
    private val tagRepository: TagRepository,
    private val webScoketController: WebscoketController
) {

    fun     updateTag(tags: List<Tag>) {
        val exisitingTags: List<TagEntity> = tagRepository.findByDateEquals(LocalDate.now())
        exisitingTags.forEach { exisitingTag ->
            val newTag = tags.findLast { it.name == exisitingTag.name }
            exisitingTag.count += newTag?.count ?: 0
        }
        val newTags: List<TagEntity> = tags.filter { tag -> !exisitingTags.map { it.name }.contains(tag.name) }.map { TagEntity(name = it.name, count = it.count, date = LocalDate.now()) }
        tagRepository.saveAll(exisitingTags);
        tagRepository.saveAll(newTags)
        webScoketController.send(exisitingTags.union(newTags).map { Tag(it.name, it.count) });
    }
}
