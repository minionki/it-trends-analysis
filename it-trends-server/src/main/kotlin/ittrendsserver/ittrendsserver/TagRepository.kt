package ittrendsserver.ittrendsserver

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity(name = "tag")
class TagEntity(

    @Id
    @GeneratedValue
    @Column(name = "id")
    var id: Int? = null,

    @Column(name = "name")
    var name: String,

    @Column(name = "count")
    var count: Int,

    @Column(name = "date")
    var date: LocalDate
)

interface TagRepository : JpaRepository<TagEntity, Int> {

    fun findByDateEquals(date: LocalDate): List<TagEntity>;
}
