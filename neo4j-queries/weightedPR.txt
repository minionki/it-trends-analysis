CALL gds.pageRank.stream('tag-weight', {
  maxIterations: 20,
  dampingFactor: 0.85,
  relationshipWeightProperty: 'myvalue'
})
YIELD nodeId, score
RETURN gds.util.asNode(nodeId).tag AS name, score
ORDER BY score DESC, name ASC