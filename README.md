# System rekomendacji kierunków rozwoju programisty na podstawie aktywności użytkowników StackOverflow i Github

Projekt realizowany w ramach przedmiotu: Metody i systemy analizy Big Data \\

Autorzy: Łukasz Kiełbus, Michał Możdżonek, Agata Nielipińska, Łukasz Osowicki

[Link do aplikacji](http://35.240.2.255:30351/)

## Struktura projektu

* **arch** -- diagramy architektury
* **big-query** -- zapytania do BigQuery
* **doc** -- dokumentacja, raporty z testów
* **flink** -- projekt z implementacją jobów Flinka
* **for-nifi-jars** -- biblioteki umożliwiające insert danych do Neo4J z poziomu NiFi
* **hdfs-config** -- konfiguracja HDFSa
* **it-trends-app** -- aplikacja webowa w Angularze
* **it-trends-server** -- backend aplikacji w Spring Boot
* **k8s** -- konfiguracja Kubernetesa
* **neo4j-queries** -- query wywołujące algorytm
* **nifi-processors** -- projekt z imlementacją własnych procesorów NiFi
* **spark-streaming** -- deprecated, próba wykorzystania spark streaming
* **sparkScala** -- próba spark + graphX
* **tests** -- test walidacji JSONa danych z GitHub
* **flow.xml** zrzut flow z nifi