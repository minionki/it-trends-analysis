package bigdata2020.ittrend.processors.custom.stack.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


@Deprecated
@Ignore
public class OtherUtilsTest {

    @Test
    public void extractLastCreationDateTest() {
        String result = OtherUtils.extractLastCreationDate(SAMPLE_JSON);
        String expected = "1585499181";
        Assert.assertEquals(expected, result);
    }

    private final String SAMPLE_JSON = "\n" +
            "\n" +
            "{\n" +
            "  \"items\": [\n" +
            "    {\n" +
            "      \"creation_date\": 1585499180,\n" +
            "      \"event_id\": 60917318,\n" +
            "      \"event_type\": \"question_posted\",\n" +
            "      \"excerpt\": \"I need to add about 600 records from one database to another one.\\n\\nThe first part inserts from a select like this:\\n\\nINSERT INTO RelayMapper.dbo.\",\n" +
            "      \"link\": \"https://stackoverflow.com/questions/60917318/inserting-rows-from-one-database-table-into-two-different-tables-in-another-data\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"creation_date\": 1585499179,\n" +
            "      \"event_id\": 60917317,\n" +
            "      \"event_type\": \"question_posted\",\n" +
            "      \"excerpt\": \"I create CDN server with NodeJs\\nFor cache controlling, I keep last file versions in Server DB.\\nFrom client, I request to main file name, without \",\n" +
            "      \"link\": \"https://stackoverflow.com/questions/60917317/cdn-files-url-need-to-redirect-301-or-302\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"creation_date\": 1585499179,\n" +
            "      \"event_id\": 13147925,\n" +
            "      \"event_type\": \"user_created\",\n" +
            "      \"excerpt\": \"Kofoed Bjerregaard\",\n" +
            "      \"link\": \"https://stackoverflow.com/users/13147925/kofoed-bjerregaard\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"has_more\": true,\n" +
            "  \"quota_max\": 10000,\n" +
            "  \"quota_remaining\": 9988,\n" +
            "  \"page\": 1,\n" +
            "  \"page_size\": 3,\n" +
            "  \"total\": 694,\n" +
            "  \"type\": \"event\"\n" +
            "}\n" +
            "\n";
}