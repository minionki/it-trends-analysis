package bigdata2020.ittrend.processors.custom.stack;

import bigdata2020.ittrend.processors.custom.stack.model.RawEvent;
import bigdata2020.ittrend.processors.custom.stack.model.RawEventType;
import bigdata2020.ittrend.processors.custom.stack.util.LinkUtils;
import bigdata2020.ittrend.processors.custom.stack.util.OtherUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ExtractIdProcessorTest {

    private final ExtractIdProcessor processor = new ExtractIdProcessor();

    @Test
    public void extractAnswerIdTest() {
        String link = "https://stackoverflow.com/questions/60937408/android-drawable-importer-not-working-in-android-studio/60937544#60937544";
        Long expected = 60937544L;
        Long actual = LinkUtils.answerIdFrom(link);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void parseEventListTest() {
        String json = "{\"items\":[{\"creation_date\":1587145171,\"event_id\":108403640,\"event_type\":\"comment_posted\",\"excerpt\":\"Good eye @Carcigenicate — I was staring at this an could see nothing.\",\"link\":\"https://stackoverflow.com/questions/61277387/python-composition-getting-an-attribute-error-for-object-inside-the-class#comment108403640_61277387\"}],\"has_more\":true,\"quota_max\":10000,\"quota_remaining\":6230,\"page\":1,\"page_size\":1,\"total\":18,\"type\":\"event\"}";
        List<RawEvent> eventList = processor.mapToRawEventList(json);

        Assert.assertEquals(1, eventList.size());
        RawEvent event = eventList.get(0);

        Assert.assertEquals(1587145171L, event.getCreationDate().longValue());
        Assert.assertEquals(108403640L, event.getEventId().longValue());
        Assert.assertEquals(RawEventType.COMMENT_POSTED, event.getType());
    }

    @Test
    public void parseEmptyEventListTest() {
        String json = "{\"items\":[],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":6225,\"page\":1,\"page_size\":1,\"total\":0,\"type\":\"event\"}";
        List<RawEvent> eventList = processor.mapToRawEventList(json);

        Assert.assertEquals(0, eventList.size());
    }

    @Test
    public void mapToAnswerIdList() {
        String json = "{\"items\":[{\"creation_date\":1587145816,\"event_id\":61277661,\"event_type\":\"answer_posted\",\"excerpt\":\"You passed token in props\\n\\n&lt;CustomDrawerMenu {...this.props} token={new Date()} /&gt;\\r\\nSo, why it should be inside this.props.navigation ?\\n\\nIt's \",\"link\":\"https://stackoverflow.com/questions/61277389/undefined-this-props-navigation-getparam-in-drawer-custom-menu/61277661#61277661\"},{\"creation_date\":1587145815,\"event_id\":61277660,\"event_type\":\"answer_posted\",\"excerpt\":\"You can disable warnings at the project level.\\n\\nIf you're using rollup, just handle onwarn:\\n\\nimport svelte from 'rollup-plugin-svelte'\\n\\nexport default \",\"link\":\"https://stackoverflow.com/questions/61272669/how-can-i-systematically-disable-certain-irrelevant-a11y-warnings-when-compiling/61277660#61277660\"},{\"creation_date\":1587145814,\"event_id\":61277659,\"event_type\":\"question_posted\",\"excerpt\":\"this is one of my first apps in xamarin format. I need to modify a list order but I don't find the way. I need for it to be shown in order by \",\"link\":\"https://stackoverflow.com/questions/61277659/change-the-order-in-listview\"},{\"creation_date\":1587145813,\"event_id\":108403994,\"event_type\":\"comment_posted\",\"excerpt\":\"Hey I am facing Problem. That transformable attributes does not sync with iCloud stackoverflow.com/questions/61276524/&hellip;. Please look on this\",\"link\":\"https://stackoverflow.com/questions/58671487/encrypting-coredata-with-cloudkit#comment108403994_58671487\"}],\"has_more\":true,\"quota_max\":10000,\"quota_remaining\":6215,\"page\":1,\"page_size\":4,\"total\":351,\"type\":\"event\"}";
        List<RawEvent> eventList = processor.mapToRawEventList(json);

        Assert.assertEquals(4, eventList.size());

        String idList = processor.mapToIdList(eventList);
        Assert.assertEquals("61277661%3B61277660", idList);
    }

    @Test
    public void mapToEmptyAnswerIdList() {
        String json = "{\"items\":[{\"creation_date\":1587145813,\"event_id\":108403994,\"event_type\":\"comment_posted\",\"excerpt\":\"Hey I am facing Problem. That transformable attributes does not sync with iCloud stackoverflow.com/questions/61276524/&hellip;. Please look on this\",\"link\":\"https://stackoverflow.com/questions/58671487/encrypting-coredata-with-cloudkit#comment108403994_58671487\"}],\"has_more\":true,\"quota_max\":10000,\"quota_remaining\":6215,\"page\":1,\"page_size\":1,\"total\":351,\"type\":\"event\"}";
        List<RawEvent> eventList = processor.mapToRawEventList(json);

        Assert.assertEquals(1, eventList.size());

        String idList = processor.mapToIdList(eventList);
        Assert.assertEquals("1", idList);
    }

    @Test
    public void extractLastCreationDateTest() {
        String json = "{\"items\":[{\"creation_date\":1587145813,\"event_id\":108403994,\"event_type\":\"comment_posted\",\"excerpt\":\"Hey I am facing Problem. That transformable attributes does not sync with iCloud stackoverflow.com/questions/61276524/&hellip;. Please look on this\",\"link\":\"https://stackoverflow.com/questions/58671487/encrypting-coredata-with-cloudkit#comment108403994_58671487\"}],\"has_more\":true,\"quota_max\":10000,\"quota_remaining\":6215,\"page\":1,\"page_size\":1,\"total\":351,\"type\":\"event\"}";
        String lastCreationDate = OtherUtils.extractLastCreationDate(json);

        Assert.assertEquals("1587145814", lastCreationDate);
    }

    @Test
    public void extractLastCreationDateEmptyTest() {
        String json = "{\"items\":[],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":6225,\"page\":1,\"page_size\":1,\"total\":0,\"type\":\"event\"}";
        String lastCreationDate = OtherUtils.extractLastCreationDate(json);

        Assert.assertEquals("1", lastCreationDate);
    }
}