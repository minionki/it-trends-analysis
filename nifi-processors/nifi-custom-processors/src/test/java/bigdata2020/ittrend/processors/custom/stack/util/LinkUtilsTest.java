package bigdata2020.ittrend.processors.custom.stack.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@Deprecated
@Ignore
public class LinkUtilsTest {

    @Test
    public void extractQuestionIdCorrectly() {
        String link = "https://stackoverflow.com/questions/60937547/how-to-export-api-data-from-python-into-excel-table";
        Long expected = 60937547L;
        Long actual = LinkUtils.questionIdFrom(link);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void extractAnswerIdCorrectly() {
        String link = "https://stackoverflow.com/questions/60937408/android-drawable-importer-not-working-in-android-studio/60937544#60937544";
        Long expected = 60937544L;
        Long actual = LinkUtils.answerIdFrom(link);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void extractCommentIdCorrectly() {
        String link = "https://stackoverflow.com/questions/60936240/django-manytomanyfield-query-equalivent-to-a-list/60936405#comment107810509_60936405";
        Long expected = 60936405L;
        Long actual = LinkUtils.commentIdFrom(link);
        Assert.assertEquals(expected, actual);
    }
}