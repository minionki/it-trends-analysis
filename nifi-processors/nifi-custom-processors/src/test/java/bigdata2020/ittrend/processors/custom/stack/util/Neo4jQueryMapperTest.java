package bigdata2020.ittrend.processors.custom.stack.util;

import bigdata2020.ittrend.processors.custom.stack.model.PostType;
import bigdata2020.ittrend.processors.custom.stack.model.QueueEvent;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

@Deprecated
@Ignore
public class Neo4jQueryMapperTest {

    private final List<QueueEvent> eventList = Arrays.asList(
            QueueEvent.builder()
                    .id(1L)
                    .questionId(1L)
                    .type(PostType.QUESTION)
                    .timestamp(1L)
                    .toBeChecked(false)
                    .build(),
            QueueEvent.builder()
                    .id(2L)
                    .questionId(1L)
                    .type(PostType.ANSWER)
                    .timestamp(1L)
                    .toBeChecked(false)
                    .build(),
            QueueEvent.builder()
                    .id(3L)
                    .questionId(1L)
                    .type(PostType.COMMENT)
                    .timestamp(1L)
                    .toBeChecked(false)
                    .build()
    );

    private final List<Long> questionIdList = Arrays.asList(1L, 2L, 3L);

    @Test
    public void createManyNodesTest() {
        String createQuery = Neo4jQueryMapper.createManyNodes(eventList);
        String expectedQuery = "CREATE ({type:\"QUESTION\",id:1,questionId:1,toBeChecked:false,timestamp:1}),({type:\"ANSWER\",id:2,questionId:1,toBeChecked:false,timestamp:1}),({type:\"COMMENT\",id:3,questionId:1,toBeChecked:false,timestamp:1})";
        Assert.assertEquals(expectedQuery, createQuery);
        System.out.println(createQuery);
    }

    @Test
    public void createManyQueueEventNodesTest() {
        String createQuery = Neo4jQueryMapper.createManyQueueEventNodes(eventList);
        String expectedQuery = "CREATE (x1:QueueEvent{type:\"QUESTION\",id:1,questionId:1,toBeChecked:false,timestamp:1}),(x2:QueueEvent{type:\"ANSWER\",id:2,questionId:1,toBeChecked:false,timestamp:1}),(x3:QueueEvent{type:\"COMMENT\",id:3,questionId:1,toBeChecked:false,timestamp:1})";
        Assert.assertEquals(expectedQuery, createQuery);
        System.out.println(createQuery);
    }

    @Test
    public void setManyQueueEventToCheckTrueTest() {
        String setQuery = Neo4jQueryMapper.setManyQueueEventToCheck(questionIdList);
        String expectedQuery = "MATCH (e:QueueEvent) WHERE e.questionId IN [1,2,3] SET e.toBeChecked = true return e.id";
        Assert.assertEquals(expectedQuery, setQuery);
        System.out.println(setQuery);
    }

    @Test
    public void setManyQueueEventToCheckFalseTest() {
        // TODO FIX
//        String setQuery = Neo4jQueryMapper.setManyQueueEventToCheck(questionIdList, false);
//        String expectedQuery = "MATCH (e:QueueEvent) WHERE e.questionId IN [1,2,3] SET e.toBeChecked = false return e.id";
//        Assert.assertEquals(expectedQuery, setQuery);
//        System.out.println(setQuery);
    }

}