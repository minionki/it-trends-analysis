package bigdata2020.ittrend.processors.custom.stack;

import bigdata2020.ittrend.processors.custom.AbstractCustomProcessor;
import bigdata2020.ittrend.processors.custom.stack.model.QueueEvent;
import bigdata2020.ittrend.processors.custom.stack.model.RawEvent;
import bigdata2020.ittrend.processors.custom.stack.model.RawEventList;
import bigdata2020.ittrend.processors.custom.stack.util.LinkUtils;
import bigdata2020.ittrend.processors.custom.stack.util.Neo4jQueryMapper;
import bigdata2020.ittrend.processors.custom.stack.util.OtherUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;

import java.util.*;
import java.util.stream.Collectors;

import static bigdata2020.ittrend.processors.custom.stack.MapEventProcessor.DESCRIPTION;
import static bigdata2020.ittrend.processors.custom.stack.common.Constants.*;

@Tags({TAG_CUSTOM})
@CapabilityDescription(DESCRIPTION)
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class MapEventProcessor extends AbstractCustomProcessor {

    static final String DESCRIPTION = "Map and filter received StackExchange GET /events to model stored in Neo4j";

    private final ObjectMapper mapper = new ObjectMapper();

    private static final Relationship RELATIONSHIP_FOR_INSERT = new Relationship.Builder()
            .name(RELATIONSHIP_NAME_FOR_INSERT)
            .description(MAP_EVENT_FOR_INSERT)
            .build();

    private static final Relationship RELATIONSHIP_FOR_UPDATE = new Relationship.Builder()
            .name(RELATIONSHIP_NAME_FOR_UPDATE)
            .description(MAP_EVENT_FOR_UPDATE)
            .build();

    private static final Relationship RELATIONSHIP_LAST_CREATION_DATE = new Relationship.Builder()
            .name(RELATIONSHIP_NAME_LAST_CREATION_DATE)
            .description(MAP_EVENT_LAST_CREATION_DATE)
            .build();


    @Override
    protected void init(final ProcessorInitializationContext context) {
        final Set<Relationship> relationships = new HashSet<>(Arrays.asList(
                RELATIONSHIP_FOR_INSERT,
                RELATIONSHIP_FOR_UPDATE,
                RELATIONSHIP_LAST_CREATION_DATE
        ));
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected void process(ProcessSession session, String content) {
        List<RawEvent> rawEventList = mapToRawEventList(content);

        mapAndTransferTo(session, RELATIONSHIP_FOR_INSERT, rawEventList, this::mapToNeo4jCreateQuery);
        mapAndTransferTo(session, RELATIONSHIP_FOR_UPDATE, rawEventList, this::mapToNeo4jUpdateQuery);
        mapAndTransferTo(session, RELATIONSHIP_LAST_CREATION_DATE, content, OtherUtils::extractLastCreationDate);
    }

    private List<RawEvent> mapToRawEventList(String content) {
        try {
            return mapper.readValue(content, RawEventList.class).getItems();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private String mapToNeo4jCreateQuery(List<RawEvent> rawEventList) {
        List<QueueEvent> queueEventList = rawEventList.stream()
                .filter(RawEvent::isPosted)
                .map(RawEvent::mapToQueueEvent)
                .distinct()
                .collect(Collectors.toList());

        return Neo4jQueryMapper.createManyQueueEventNodes(queueEventList);
    }

    private String mapToNeo4jUpdateQuery(List<RawEvent> rawEventList) {
        List<Long> questionIdList = rawEventList.stream()
                .filter(RawEvent::requiresUpdate)
                .map(rawEvent -> LinkUtils.questionIdFrom(rawEvent.getLink()))
                .distinct()
                .collect(Collectors.toList());

        return Neo4jQueryMapper.setManyQueueEventToCheck(questionIdList);
    }
}
