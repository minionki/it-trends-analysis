package bigdata2020.ittrend.processors.custom.stack.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdMapModel {
    @JsonProperty("e.type")
    private PostType type;

    @JsonProperty("e.id")
    private Long id;
}
