package bigdata2020.ittrend.processors.custom.stack.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RawEventType {
    QUESTION_POSTED,
    ANSWER_POSTED,
    COMMENT_POSTED,
    POST_EDITED,
    USER_CREATED;

    @JsonCreator
    public static RawEventType forValue(String value) {
        return RawEventType.valueOf(value.toUpperCase());
    }

    @JsonValue
    public String toValue() {
        return this.name().toLowerCase();
    }
}
