package bigdata2020.ittrend.processors.custom.stack.model;


import bigdata2020.ittrend.processors.custom.stack.util.LinkUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueueEvent {
    private PostType type;
    private Long id;
    private Long questionId;
    private boolean toBeChecked;
    private Long timestamp;

    public static QueueEvent questionOf(RawEvent rawEvent) {
        Long questionId = LinkUtils.questionIdFrom(rawEvent.getLink());
        return QueueEvent.builder()
                .type(PostType.QUESTION)
                .id(questionId)
                .questionId(questionId)
                .timestamp(rawEvent.getCreationDate())
                .toBeChecked(false)
                .build();
    }

    public static QueueEvent answerOf(RawEvent rawEvent) {
        Long questionId = LinkUtils.questionIdFrom(rawEvent.getLink());
        Long commentId = LinkUtils.answerIdFrom(rawEvent.getLink());
        return QueueEvent.builder()
                .type(PostType.ANSWER)
                .id(commentId)
                .questionId(questionId)
                .timestamp(rawEvent.getCreationDate())
                .toBeChecked(false)
                .build();
    }

    public static QueueEvent commentOf(RawEvent rawEvent) {
        Long questionId = LinkUtils.questionIdFrom(rawEvent.getLink());
        Long commentId = LinkUtils.commentIdFrom(rawEvent.getLink());
        return QueueEvent.builder()
                .type(PostType.COMMENT)
                .id(commentId)
                .questionId(questionId)
                .timestamp(rawEvent.getCreationDate())
                .toBeChecked(false)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueueEvent that = (QueueEvent) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
