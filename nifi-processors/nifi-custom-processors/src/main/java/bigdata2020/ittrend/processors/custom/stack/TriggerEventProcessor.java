package bigdata2020.ittrend.processors.custom.stack;

import bigdata2020.ittrend.processors.custom.AbstractCustomProcessor;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static bigdata2020.ittrend.processors.custom.stack.TriggerEventProcessor.DESCRIPTION;
import static bigdata2020.ittrend.processors.custom.stack.common.Constants.*;

@Tags({TAG_CUSTOM})
@CapabilityDescription(DESCRIPTION)
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class TriggerEventProcessor extends AbstractCustomProcessor {

    static final String DESCRIPTION = "Trigger GET /events if no 'since' property is present in queue";

    private static final Relationship RELATIONSHIP_SINCE = new Relationship.Builder()
            .name(RELATIONSHIP_NAME_SINCE)
            .autoTerminateDefault(true)
            .description(TRIGGER_EVENT_SINCE)
            .build();

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final Set<Relationship> relationships = new HashSet<>(Collections.singletonList(RELATIONSHIP_SINCE));
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public void onTrigger(ProcessContext processContext, ProcessSession session) throws ProcessException {
        mapAndTransferTo(session, RELATIONSHIP_SINCE, "", c -> "1");
    }

    @Override
    protected void process(ProcessSession session, String content) {
    }
}
