package bigdata2020.ittrend.processors.custom.stack.model;

public enum PostType {
    QUESTION,
    ANSWER,
    COMMENT
}
