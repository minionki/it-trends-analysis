package bigdata2020.ittrend.processors.custom.stack.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class RawEventList {
    private List<RawEvent> items;

    @JsonProperty("has_more")
    private boolean hasMore;

    @JsonProperty("quota_max")
    private int quotaMax;

    @JsonProperty("quota_remaining")
    private int quotaRemaining;

    private int page;

    @JsonProperty("page_size")
    private int pageSize;

    @JsonProperty("total")
    private int total;

    private String type;
}
