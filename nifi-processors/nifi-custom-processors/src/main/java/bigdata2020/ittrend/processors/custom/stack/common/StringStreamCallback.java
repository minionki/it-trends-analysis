package bigdata2020.ittrend.processors.custom.stack.common;

import org.apache.nifi.processor.io.StreamCallback;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class StringStreamCallback implements StreamCallback {

    private final StringMapper mapper;

    public StringStreamCallback(StringMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void process(InputStream inputStream, OutputStream outputStream) throws IOException {
        try (
                Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name());
                OutputStreamWriter writer = new OutputStreamWriter(outputStream)
        ) {
            String content = scanner.useDelimiter("\\A").next();
            String lastCreationDate = mapper.map(content);
            writer.write(lastCreationDate);
        }
    }
}

