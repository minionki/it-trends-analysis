package bigdata2020.ittrend.processors.custom.stack;


import bigdata2020.ittrend.processors.custom.AbstractCustomProcessor;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static bigdata2020.ittrend.processors.custom.stack.CustomDebugProcessor.DESCRIPTION;
import static bigdata2020.ittrend.processors.custom.stack.common.Constants.*;

@Tags({TAG_CUSTOM})
@CapabilityDescription(DESCRIPTION)
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class CustomDebugProcessor extends AbstractCustomProcessor {

    static final String DESCRIPTION = "Just LOG.info content of FlowFile";

    private static final Relationship RELATIONSHIP_ORIGINAL = new Relationship.Builder()
            .name(RELATIONSHIP_NAME_ORIGINAL)
            .autoTerminateDefault(true)
            .description(ORIGINAL_FLOW_FILE)
            .build();

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final Set<Relationship> relationships = new HashSet<>(Collections.singletonList(RELATIONSHIP_ORIGINAL));
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected void process(ProcessSession session, String content) {
        mapAndTransferTo(session, RELATIONSHIP_ORIGINAL, content, this::debugAndPush);
    }

    private String debugAndPush(String content) {
        ComponentLog logger = getLogger();
        logger.info(content);
        return content;
    }
}
