package bigdata2020.ittrend.processors.custom;

import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;

public abstract class AbstractCustomProcessor extends AbstractProcessor {

    protected List<PropertyDescriptor> descriptors = Collections.emptyList();
    protected Set<Relationship> relationships = Collections.emptySet();

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @Override
    public void onTrigger(ProcessContext processContext, ProcessSession session) throws ProcessException {
        String content = readContent(session);
        process(session, content);
    }

    protected abstract void process(ProcessSession session, String content);

    protected <T> void mapAndTransferTo(ProcessSession session, Relationship relationship, T data, Function<T, String> mapper) {

        FlowFile outputFlowFile = session.create();
        OutputStream outputStream = session.write(outputFlowFile);

        String result;
        try {
            result = mapper.apply(data);
        } catch (Exception e) {
            getLogger().error(data.toString());
            result = "0";
        }

        try (OutputStreamWriter writer = new OutputStreamWriter(outputStream)) {
            writer.write(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

        session.transfer(outputFlowFile, relationship);
    }

    private String readContent(ProcessSession session) {

        FlowFile inputFlowFile = session.get();

        if (inputFlowFile == null) {
            return "";
        }

        InputStream inputStream = session.read(inputFlowFile);
        String content;

        try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name())) {
            content = scanner.useDelimiter("\\A").next();
        }

        session.remove(inputFlowFile); //to remove FlowFile from queue
        return content;
    }

    protected static Relationship rel(String name, String description) {
        return new Relationship.Builder()
                .name(name)
                .description(description)
                .build();
    }
}
