package bigdata2020.ittrend.processors.custom.stack.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static bigdata2020.ittrend.processors.custom.stack.model.RawEventType.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RawEvent {


    @JsonProperty("creation_date")
    private Long creationDate;

    @JsonProperty("event_id")
    private Long eventId;

    @JsonProperty("event_type")
    private RawEventType type;

    private String excerpt;

    private String link;

    public boolean isPosted() {
        return type == QUESTION_POSTED
                || type == ANSWER_POSTED
                || type == COMMENT_POSTED;
    }

    public boolean isAnswer() {
        return type == ANSWER_POSTED;
    }

    public boolean requiresUpdate() {
        return type == ANSWER_POSTED
                || type == COMMENT_POSTED
                || type == POST_EDITED;
    }

    public QueueEvent mapToQueueEvent() {
        switch (type) {
            case QUESTION_POSTED: return QueueEvent.questionOf(this);
            case ANSWER_POSTED: return QueueEvent.answerOf(this);
            case COMMENT_POSTED: return QueueEvent.commentOf(this);
        }
        throw new IllegalStateException("Only QUESTION_POSTED, ANSWER_POSTED and COMMENT_POSTED raw events can be mapped to QueueEvent");
    }

}
