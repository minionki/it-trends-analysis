package bigdata2020.ittrend.processors.custom.stack.common;

public interface StringMapper {
    String map(String content);
}
