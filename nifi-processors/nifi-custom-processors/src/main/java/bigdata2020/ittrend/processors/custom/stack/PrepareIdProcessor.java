package bigdata2020.ittrend.processors.custom.stack;

import bigdata2020.ittrend.processors.custom.AbstractCustomProcessor;
import bigdata2020.ittrend.processors.custom.stack.model.IdMapModel;
import bigdata2020.ittrend.processors.custom.stack.model.PostType;
import bigdata2020.ittrend.processors.custom.stack.util.Neo4jQueryMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;

import java.util.*;
import java.util.stream.Collectors;

import static bigdata2020.ittrend.processors.custom.stack.common.Constants.TAG_CUSTOM;

@Tags({TAG_CUSTOM})
@CapabilityDescription(PrepareIdProcessor.DESCRIPTION)
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class PrepareIdProcessor extends AbstractCustomProcessor {

    static final String DESCRIPTION = "Prepare id for InvokeHttp and Neo4j";

    private static final String COMMENTS = "comments";
    private static final String ANSWERS = "answers";
    private static final String QUESTIONS = "questions";

    private static final String COMMENTS_KEY = "fgfCnpfFfBTsTFgQrJHUrg%28%28";
    private static final String ANSWERS_KEY = "gtcPe46BA)Sprrf40oN47w%28%28";
    private static final String QUESTIONS_KEY = "UExi9fwJce*fugY220ILyw%28%28";

    private static final String COMMENTS_TOKEN = "a*dsmWXHgI12WGk*niS*Kw%29%29";
    private static final String ANSWERS_TOKEN = "0n*yxn84oqkg9jFtgXRRsg%29%29";
    private static final String QUESTIONS_TOKEN = "10tkFEG7xRorM8ax9hNZEw%29%29";

    private static final String URL_DESC = "Url for InvokeHttp - GET /questions /answers /comments";
    private static final String FOR_UNCHECK_DESC = "Data for Neo4j update - marking selected ids 'toBeChecked' to false";

    private static final Relationship RELATIONSHIP_URL = rel("url", URL_DESC);
    private static final Relationship RELATIONSHIP_FOR_UNCHECK = rel("for-uncheck", FOR_UNCHECK_DESC);

    private final ObjectMapper mapper = new ObjectMapper();
    private final CollectionType idModelListType = mapper.getTypeFactory().constructCollectionType(List.class, IdMapModel.class);

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final Set<Relationship> relationships = new HashSet<>(Arrays.asList(RELATIONSHIP_URL, RELATIONSHIP_FOR_UNCHECK));
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected void process(ProcessSession session, String content) {
        List<IdMapModel> idModelList = mapToModelList(content);
        mapAndTransferTo(session, RELATIONSHIP_URL, idModelList, this::mapToGetQuestions);
        mapAndTransferTo(session, RELATIONSHIP_URL, idModelList, this::mapToGetAnswers);
        mapAndTransferTo(session, RELATIONSHIP_URL, idModelList, this::mapToGetComments);
        mapAndTransferTo(session, RELATIONSHIP_FOR_UNCHECK, idModelList, this::mapToNeo4jUpdateQuery);
    }

    private List<IdMapModel> mapToModelList(String content) {
        try {
            return mapper.readValue(content, idModelListType);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private String mapToGetQuestions(List<IdMapModel> idModelList) {
        List<String> idList = filterAndMapIdList(idModelList, PostType.QUESTION);
        return mapToHttpGetUrl(idList, QUESTIONS, QUESTIONS_KEY, QUESTIONS_TOKEN);
    }

    private String mapToGetAnswers(List<IdMapModel> idModelList) {
        List<String> idList = filterAndMapIdList(idModelList, PostType.ANSWER);
        return mapToHttpGetUrl(idList, ANSWERS, ANSWERS_KEY, ANSWERS_TOKEN);
    }

    private String mapToGetComments(List<IdMapModel> idModelList) {
        List<String> idList = filterAndMapIdList(idModelList, PostType.COMMENT);
        return mapToHttpGetUrl(idList, COMMENTS, COMMENTS_KEY, COMMENTS_TOKEN);
    }

    private String mapToHttpGetUrl(List<String> idList, String type, String key, String accessToken) {
        return "https://api.stackexchange.com/2.2/" + type + "/" + String.join("%3B", idList) +
                "?site=stackoverflow&key=" + key +
                "&access_token=" + accessToken;
    }

    private List<String> filterAndMapIdList(List<IdMapModel> idModelList, PostType type) {
        List<String> idList = idModelList.stream()
                .filter(model -> model.getType() == type)
                .map(model -> model.getId().toString())
                .collect(Collectors.toList());
        if (idList.isEmpty()) {
            idList.add("1"); //we need to have some id in query, otherwise results will not be filtered
        }
        return idList;
    }

    private String mapToNeo4jUpdateQuery(List<IdMapModel> idModelList) {
        List<Long> idList = idModelList.stream().map(IdMapModel::getId).collect(Collectors.toList());
        return Neo4jQueryMapper.setManyQueueEventToUnCheck(idList);
    }
}
