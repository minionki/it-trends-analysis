package bigdata2020.ittrend.processors.custom.stack.util;

public class LinkUtils {

    // WARNING this mappings may be wrong for event post_edited (this event is not supported for now)

    public static Long questionIdFrom(String link) {
        int questionsStringIndex = link.indexOf("questions");
        int questionIdStart = link.indexOf('/', questionsStringIndex) + 1; // link looks: .../questions/{questionId}/
        int questionIdEnd = link.indexOf('/', questionIdStart);
        String questionId = link.substring(questionIdStart, questionIdEnd);
        return Long.valueOf(questionId);
    }

    public static Long answerIdFrom(String link) {
        int answerIdStart = link.lastIndexOf('#') + 1; // link looks: .../{answerId}#{answerId}"
        int answerIdEnd = link.length();
        String answerId = link.substring(answerIdStart, answerIdEnd);
        return Long.valueOf(answerId);
    }

    public static Long commentIdFrom(String link) {
        int commentStringIndex = link.indexOf("#comment");
        int commentIdStart = link.indexOf('_', commentStringIndex) + 1; // link looks: ...#comment{questionOrAnswerId}_{commentId}"
        int commendIdEnd = link.length();
        String commentId = link.substring(commentIdStart, commendIdEnd);
        return Long.valueOf(commentId);
    }
}
