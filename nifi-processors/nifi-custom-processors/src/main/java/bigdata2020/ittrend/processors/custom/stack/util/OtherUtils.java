package bigdata2020.ittrend.processors.custom.stack.util;

public class OtherUtils {

    public static String extractLastCreationDate(String content) {
        int firstCreationDateIndex = content.indexOf("creation_date");
        if (firstCreationDateIndex == -1) {
            return "1";
        }
        int startOfFirstCreationDateIndex = content.indexOf(':', firstCreationDateIndex) + 1;
        int endOfFirstCreationDateIndex = content.indexOf(',', firstCreationDateIndex);
        String lastCreationDateString = content.substring(startOfFirstCreationDateIndex, endOfFirstCreationDateIndex).trim();
        long lastCreationDate = Long.valueOf(lastCreationDateString);
        return Long.toString(lastCreationDate + 1);
    }
}
