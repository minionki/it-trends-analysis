package bigdata2020.ittrend.processors.custom.stack.common;

public class Constants {

    public static final String TAG_CUSTOM = "custom";
    public static final String RELATIONSHIP_NAME_SUCCESS = "success";
    public static final String RELATIONSHIP_NAME_ORIGINAL = "original";
    public static final String RELATIONSHIP_NAME_SINCE = "since";
    public static final String RELATIONSHIP_NAME_FOR_INSERT = "for-insert";
    public static final String RELATIONSHIP_NAME_FOR_UPDATE = "for-update";
    public static final String RELATIONSHIP_NAME_LAST_CREATION_DATE = "last-creation-date";

    public static final String MAP_EVENT_FOR_INSERT = "New questions, answers and commends added to queue";
    public static final String MAP_EVENT_FOR_UPDATE = "Data for Neo4j update - marking selected ids as 'toBeChecked'";
    public static final String MAP_EVENT_LAST_CREATION_DATE = "Last creation date for next event fetching query";

    public static final String ORIGINAL_FLOW_FILE = "Original flowFile";

    public static final String TRIGGER_EVENT_SINCE = "Gives flowFile with 'since' property to trigger GET /eventss";
}
