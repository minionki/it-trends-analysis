package bigdata2020.ittrend.processors.custom.stack;

import bigdata2020.ittrend.processors.custom.AbstractCustomProcessor;
import bigdata2020.ittrend.processors.custom.stack.model.RawEvent;
import bigdata2020.ittrend.processors.custom.stack.model.RawEventList;
import bigdata2020.ittrend.processors.custom.stack.util.LinkUtils;
import bigdata2020.ittrend.processors.custom.stack.util.OtherUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;

import java.util.*;
import java.util.stream.Collectors;

import static bigdata2020.ittrend.processors.custom.stack.ExtractIdProcessor.DESCRIPTION;
import static bigdata2020.ittrend.processors.custom.stack.common.Constants.TAG_CUSTOM;

@Tags({TAG_CUSTOM})
@CapabilityDescription(DESCRIPTION)
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class ExtractIdProcessor extends AbstractCustomProcessor {

    static final String DESCRIPTION = "Extract questions ids";

    private final ObjectMapper mapper = new ObjectMapper();

    private static final Relationship RELATIONSHIP_ID_LIST = new Relationship.Builder()
            .name("id-list")
            .description("Semicolon separated list of question ids, encoded for URL")
            .build();

    private static final Relationship RELATIONSHIP_LAST_CREATION_DATE = new Relationship.Builder()
            .name("last-creation-date")
            .description("Last creation date for next event fetching query")
            .build();

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final Set<Relationship> relationships = new HashSet<>(Arrays.asList(
                RELATIONSHIP_ID_LIST,
                RELATIONSHIP_LAST_CREATION_DATE
        ));
        this.relationships = Collections.unmodifiableSet(relationships);
    }


    @Override
    protected void process(ProcessSession session, String content) {
        List<RawEvent> rawEventList = mapToRawEventList(content);
        mapAndTransferTo(session, RELATIONSHIP_ID_LIST, rawEventList, this::mapToIdList);
        mapAndTransferTo(session, RELATIONSHIP_LAST_CREATION_DATE, content, OtherUtils::extractLastCreationDate);
    }

    List<RawEvent> mapToRawEventList(String content) {
        try {
            return mapper.readValue(content, RawEventList.class).getItems();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    String mapToIdList(List<RawEvent> rawEventList) {
        String idList = rawEventList.stream()
                .filter(RawEvent::isAnswer)
                .map(rawEvent -> LinkUtils.answerIdFrom(rawEvent.getLink()))
                .distinct()
                .map(Object::toString)
                .collect(Collectors.joining("%3B"));
        if (idList.equals("")) {
            return "1";
        } else {
            return idList;
        }
    }

}
