package bigdata2020.ittrend.processors.custom.stack.util;

import bigdata2020.ittrend.processors.custom.stack.model.QueueEvent;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.stream.Collectors;

public class Neo4jQueryMapper {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
    }

    public static String createManyQueueEventNodes(List<QueueEvent> eventList) {
        return eventList.stream()
                .map(Neo4jQueryMapper::queueEventToCreateNeo4j)
                .collect(Collectors.joining("),(", "CREATE (", ")"));
    }

    public static String setManyQueueEventToCheck(List<Long> questionIdList) {
        String prefix = "MATCH (e:QueueEvent) WHERE e.questionId IN [";
        String suffix = "] SET e.toBeChecked = true return e.id";
        return questionIdList.stream()
                .map(l -> Long.toString(l))
                .collect(Collectors.joining(",", prefix, suffix));
    }

    public static String setManyQueueEventToUnCheck(List<Long> idList) {
        String prefix = "MATCH (e:QueueEvent) WHERE e.id IN [";
        String suffix = "] SET e.toBeChecked = false return e.id";
        return idList.stream()
                .map(l -> Long.toString(l))
                .collect(Collectors.joining(",", prefix, suffix));
    }

    public static <T> String createManyNodes(List<T> nodes) {
        return nodes.stream()
                .map(Neo4jQueryMapper::toJson)
                .collect(Collectors.joining("),(", "CREATE (", ")"));
    }

    private static String queueEventToCreateNeo4j(QueueEvent event) {
        try {
            return "x" + event.getId() + ":QueueEvent" + MAPPER.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static <T> String toJson(T model) {
        try {
            return MAPPER.writeValueAsString(model);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
