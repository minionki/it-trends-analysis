import com.mongodb.spark.MongoSpark;
import org.apache.nifi.remote.client.SiteToSiteClientConfig;
import org.apache.nifi.spark.NiFiDataPacket;
import org.apache.nifi.spark.NiFiReceiver;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.bson.Document;

import java.nio.charset.StandardCharsets;

public class Main {

    private static final Duration DURATION = new Duration(10000L);
    private static final StorageLevel STORAGE_LEVEL = StorageLevel.MEMORY_ONLY();

    public static void main(String[] args) throws InterruptedException {

        SiteToSiteClientConfig s2sConfig = Config.defaultS2sConfig();
        SparkConf sparkConf = Config.defaultSparkConf();
        JavaStreamingContext jsc = new JavaStreamingContext(sparkConf, DURATION);
        JavaDStream<NiFiDataPacket> packetStream = jsc.receiverStream(new NiFiReceiver(s2sConfig, STORAGE_LEVEL));
        JavaDStream<String> stream = packetStream.map(new PacketToStringMapper());

        stream
                .map(new StringToDocumentMapper())
                .foreachRDD((VoidFunction<JavaRDD<Document>>) MongoSpark::save);
        jsc.start();
        jsc.awaitTermination();
    }

    private static class PacketToStringMapper implements Function<NiFiDataPacket, String> {

        @Override
        public String call(NiFiDataPacket niFiDataPacket) {
            return new String(niFiDataPacket.getContent(), StandardCharsets.UTF_8);
        }
    }

    private static class StringToDocumentMapper implements Function<String, Document> {

        @Override
        public Document call(String content) {
            return Document.parse(content);
        }
    }
}
