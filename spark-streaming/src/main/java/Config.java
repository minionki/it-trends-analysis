import org.apache.nifi.remote.client.SiteToSiteClient;
import org.apache.nifi.remote.client.SiteToSiteClientConfig;
import org.apache.nifi.remote.protocol.SiteToSiteTransportProtocol;
import org.apache.spark.SparkConf;

import static org.apache.nifi.remote.protocol.SiteToSiteTransportProtocol.HTTP;

public class Config {

    private static final String NIFI_URL = "http://10.100.4.195:8080/nifi/";
    private static final String PORT_NAME = "SparkData2";
    private static final SiteToSiteTransportProtocol TRANSPORT_PROTOCOL = HTTP;

    private static final String MASTER = "local[2]"; // means that we use 2 thread
    private static final String MONGO_OUT_URI_KEY = "spark.mongodb.output.uri";
    private static final String MONGO_OUT_URI_VALUE = "mongodb://admin:password@10.100.15.161:27017/admin.spark";
    private static final String APP_NAME = "NiFi-Spark Streaming example";

    public static SiteToSiteClientConfig defaultS2sConfig() {
        return new SiteToSiteClient.Builder()
                .url(NIFI_URL)
                .portName(PORT_NAME)
                .transportProtocol(TRANSPORT_PROTOCOL)
                .buildConfig();
    }

    public static SparkConf defaultSparkConf() {
        return new SparkConf()
                .set(MONGO_OUT_URI_KEY, MONGO_OUT_URI_VALUE)
                .setMaster(MASTER)
                .setAppName(APP_NAME);
    }
}
