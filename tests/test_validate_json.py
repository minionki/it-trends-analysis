import unittest
import json
import jsonschema
from jsonschema import validate

topics_schema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "array",
  "items": [
    {
        "type": "object",
        "required": ["repo_name", "topics"],
        "properties": {
            "repo_name": {"type": "string"},
            "topics": {
                "type": "object",
                "properties": {
                "names": {
                    "type": "array",
                    "items": [
                        {"type": "string"}
              ]
            }
          }
        }
      }
    }
  ]
}

contributors_schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "array",
    "items": [
        {
            "type": "object",
            "required" : ["repo_name", "contributors"],
            "properties": 
            {
                "repo_name": {"type": "string"},
                "contributors": 
                {
                    "type": "array",
                    "items": [
                        {
                            "type": "object",
                            "required": ["login"],
                            "properties": 
                            {
                                "login": { "type": "string"},
                                "id": {"type": "integer"},
                                "node_id": {"type": "string"},
                                "avatar_url": {"type": "string"},
                                "gravatar_id": {"type": "string"},
                                "url": {"type": "string"},
                                "html_url": {"type": "string"},
                                "followers_url": {"type": "string"},
                                "following_url": {"type": "string"},
                                "gists_url": {"type": "string"},
                                "starred_url": {"type": "string"},
                                "subscriptions_url": {"type": "string"},
                                "organizations_url": {"type": "string"},
                                "repos_url": {"type": "string"},
                                "events_url": {"type": "string"},
                                "received_events_url": {"type": "string"},
                                "type": {"type": "string"},
                                "site_admin": {"type": "boolean"},
                                "contributions": {"type": "integer"}
                            }
                        }
                    ]
                }
            }
        }
    ]
}


def validateJson(jsonData, validationSchema):
    try:
        validate(instance=jsonData, schema=validationSchema)
    except jsonschema.exceptions.ValidationError as err:
        return False
    return True

class Topics_test(unittest.TestCase):
    def test_len(self):
        with open("../topics.json") as f:
            topics = json.load(f)
            self.assertEqual(len(topics),1000)

    def test_validate_against_schema(self):
        with open("../topics.json") as f:
            topics = json.load(f)
            self.assertTrue(validateJson(topics,topics_schema))

class Contributors_test(unittest.TestCase):
    def test_len(self):
        with open("../contrb.json") as f:
            topics = json.load(f)
            self.assertEqual(len(topics),1000)

    def test_validate_against_schema(self):
        with open("../contrb.json") as f:
            topics = json.load(f)
            self.assertTrue(validateJson(topics,contributors_schema))



if __name__ == '__main__':
    unittest.main()
