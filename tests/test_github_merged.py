import json
path = "./github_merged.json"
print("\nReading file from path: {}\n".format(path))
with open(path) as f:
    contrb_json = json.load(f)
    print("[\u2713] FILE LOADED \n")
    print("Sample repos:")
    print(json.dumps(contrb_json['repos'][:2],indent=4))
    print("\nSample contributors:")
    print(json.dumps(contrb_json['contributors'][:2],indent=4))
    
