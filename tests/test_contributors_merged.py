import json
path = "./contributors_merged.json"
print("\nReading file from path: {}\n".format(path))
with open(path) as f:
    contrb_json = json.load(f)
    print("[\u2713] FILE LOADED \n")
    print("\nSample contributors:")
    print(json.dumps(contrb_json[:2],indent=4))
    
