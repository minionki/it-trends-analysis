package bigdata2020.ittrend.flink.stack.count;

import bigdata2020.ittrend.flink.stack.graph.StringToAnswerMapper;
import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import org.junit.Assert;
import org.junit.Test;

public class TagCounterMapperTest {


    @Test
    public void tagCounterTest() throws Exception {
        String json = "{\"items\":[{\"tags\":[\"java\",\"kotlin\"]},{\"tags\":[\"python\",\"java\"]},{\"tags\":[\"java\"]}],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        AnswerResponse response = new StringToAnswerMapper().map(json);
        TagCounterMapper mapper = new TagCounterMapper();
        String result = mapper.map(response);
        String expectedResult = "{\"tags\":[{\"name\":\"python\",\"count\":1},{\"name\":\"java\",\"count\":3},{\"name\":\"kotlin\",\"count\":1}]}";
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void tagCounterEmptyListTest() throws Exception {
        String json = "{\"items\":[],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        AnswerResponse response = new StringToAnswerMapper().map(json);
        TagCounterMapper mapper = new TagCounterMapper();
        String result = mapper.map(response);
        String expectedResult = "{\"tags\":[]}";
        Assert.assertEquals(expectedResult, result);
    }
}