package bigdata2020.ittrend.flink.stack.count;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.junit.Assert;
import org.junit.Test;
public class HttpCallMapperTest {

    @Test
    public void prepareHttpRequestTest() {
        String json = "{\"tags\":[]}";
        HttpCallMapper mapper = new HttpCallMapper();
        HttpPost request = mapper.prepareRequest(json);
        Assert.assertEquals(ContentType.APPLICATION_JSON.toString(), request.getEntity().getContentType().getValue());
        Assert.assertEquals("http://10.100.1.154:8080/update", request.getURI().toString());
    }
}