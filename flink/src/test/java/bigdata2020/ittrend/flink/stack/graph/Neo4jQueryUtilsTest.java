package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.neo4j.TagNode;
import bigdata2020.ittrend.flink.stack.model.neo4j.TagRelation;
import bigdata2020.ittrend.flink.stack.model.neo4j.UserNode;
import bigdata2020.ittrend.flink.stack.model.neo4j.UserTagRelation;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class Neo4jQueryUtilsTest {

    private Map<String, TagRelation> connectedTags = new HashMap<>();
    private Map<String, UserTagRelation> userTags = new HashMap<>();

    {
        connectedTags.put("scala", TagRelation.builder().from("java").to("scala").count(3L).build());
        userTags.put("scala", UserTagRelation.builder().userId(2L).tag("scala").count(5L).build());
    }


    private UserNode user1 = UserNode.builder()
            .userId(1)
            .displayName("Kowalski")
            .reputation(15L)
            .acceptRate(75L)
            .tags(new HashMap<>())
            .build();

    private UserNode user2 = UserNode.builder()
            .userId(2)
            .displayName("Nowak")
            .reputation(5L)
            .tags(userTags)
            .build();

    private TagNode tag1 = TagNode.builder()
            .tag("java")
            .count(10)
            .connected(new HashMap<>())
            .build();

    private TagNode tag2 = TagNode.builder()
            .tag("scala")
            .count(5)
            .connected(connectedTags)
            .build();


    @Test
    public void mergeUserQueryTest() {
        List<UserNode> users = Arrays.asList(user1, user2);
        String query = Neo4jQueryUtils.mergeUserList(users);
        String expectedQuery = " MERGE (u1:User{userId:1}) ON CREATE SET u1.name = \"Kowalski\",u1.acceptRate = 75,u1.reputation = 15 ON MATCH SET u1.acceptRate = 75,u1.reputation = 15  MERGE (u2:User{userId:2}) ON CREATE SET u2.name = \"Nowak\",u2.acceptRate = null,u2.reputation = 5 ON MATCH SET u2.acceptRate = null,u2.reputation = 5";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void mergeTagQueryTest() {
        List<TagNode> tags = Arrays.asList(tag1, tag2);
        String query = Neo4jQueryUtils.mergeTagList(tags);
        String expectedQuery = " MERGE (tjava:Tag{tag:'java'}) ON CREATE SET tjava.tag = \"java\",tjava.count = 10 ON MATCH SET tjava.count = tjava.count+10  MERGE (tscala:Tag{tag:'scala'}) ON CREATE SET tscala.tag = \"scala\",tscala.count = 5 ON MATCH SET tscala.count = tscala.count+5";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void mergeUserTagRelationQueryTest() {
        List<UserNode> users = Arrays.asList(user1, user2);
        String query = Neo4jQueryUtils.mergeUserTagRelations(users);
        String expectedQuery = " MERGE (tscala)-[tscalau2:Knows]-(u2)  ON CREATE SET tscalau2.count = 5 ON MATCH SET tscalau2.count = tscalau2.count+5";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void mergeTagTagRelationQueryTest() {
        List<TagNode> tags = Arrays.asList(tag1, tag2);
        String query = Neo4jQueryUtils.mergeTagRelations(tags);
        String expectedQuery = " MERGE (tjava)-[tjavatscala:Connected]-(tscala)  ON CREATE SET tjavatscala.count = 3 ON MATCH SET tjavatscala.count = tjavatscala.count+3";
        Assert.assertEquals(expectedQuery, query);
    }
}