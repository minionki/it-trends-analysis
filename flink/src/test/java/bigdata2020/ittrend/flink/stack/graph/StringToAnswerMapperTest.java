package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.json.Answer;
import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import bigdata2020.ittrend.flink.stack.model.json.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class StringToAnswerMapperTest {

    @Test
    public void parseAnswerJsonTest() {
        String json = "{\"items\":[{\"tags\":[\"java\",\"exchange-server\",\"exchangewebservices\"],\"owner\":{\"reputation\":1,\"user_id\":10337562,\"display_name\":\"Victor Marcus\"},\"last_activity_date\":1587060858,\"creation_date\":1587060858,\"answer_id\":61257140},{\"tags\":[\"python\",\"python-3.x\",\"tensorflow\"],\"owner\":{\"reputation\":46,\"user_id\":10695702,\"display_name\":\"Anchal Gupta\"},\"last_activity_date\":1587060846,\"creation_date\":1587060846,\"answer_id\":61257135}],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        StringToAnswerMapper mapper = new StringToAnswerMapper();

        AnswerResponse response = mapper.map(json);
        Assert.assertEquals(2, response.getItems().size());

        Answer answer = response.getItems().get(0);

        Assert.assertEquals(61257140L, answer.getAnswerId());
        Assert.assertEquals(1587060858L, answer.getCreationDate());


        User user = answer.getOwner();
        Assert.assertNotNull(user);
        Assert.assertEquals("Victor Marcus", user.getDisplayName());
        Assert.assertEquals(1L, user.getReputation().longValue());
        Assert.assertEquals(10337562L, user.getUserId());

        List<String> tags = answer.getTags();
        Assert.assertEquals(3, tags.size());
        Assert.assertTrue(tags.contains("java"));
        Assert.assertTrue(tags.contains("exchange-server"));
        Assert.assertTrue(tags.contains("exchangewebservices"));
    }

    @Test
    public void parseEmptyAnswerJsonTest() {
        String json = "{\"items\":[],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        StringToAnswerMapper mapper = new StringToAnswerMapper();

        AnswerResponse response = mapper.map(json);
        Assert.assertEquals(0, response.getItems().size());
    }
}