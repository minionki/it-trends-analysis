package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import org.junit.Assert;
import org.junit.Test;

public class GraphModelMapperTest {

    @Test
    public void mapJsonToQueryTest() {
        String json = "{\"items\":[{\"tags\":[\"java\",\"exchange-server\",\"exchangewebservices\"],\"owner\":{\"reputation\":1,\"user_id\":10337562,\"display_name\":\"Victor Marcus\"},\"last_activity_date\":1587060858,\"creation_date\":1587060858,\"answer_id\":61257140},{\"tags\":[\"python\",\"python-3.x\",\"tensorflow\"],\"owner\":{\"reputation\":46,\"user_id\":10695702,\"display_name\":\"Anchal Gupta\"},\"last_activity_date\":1587060846,\"creation_date\":1587060846,\"answer_id\":61257135}],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        GraphModelMapper mapper = new GraphModelMapper();
        AnswerResponse response = new StringToAnswerMapper().map(json);
        String expectedQuery = " MERGE (tpython:Tag{tag:'python'}) ON CREATE SET tpython.tag = \"python\",tpython.count = 1 ON MATCH SET tpython.count = tpython.count+1  MERGE (ttensorflow:Tag{tag:'tensorflow'}) ON CREATE SET ttensorflow.tag = \"tensorflow\",ttensorflow.count = 1 ON MATCH SET ttensorflow.count = ttensorflow.count+1  MERGE (tjava:Tag{tag:'java'}) ON CREATE SET tjava.tag = \"java\",tjava.count = 1 ON MATCH SET tjava.count = tjava.count+1  MERGE (texchangen_server:Tag{tag:'exchange-server'}) ON CREATE SET texchangen_server.tag = \"exchange-server\",texchangen_server.count = 1 ON MATCH SET texchangen_server.count = texchangen_server.count+1  MERGE (texchangewebservices:Tag{tag:'exchangewebservices'}) ON CREATE SET texchangewebservices.tag = \"exchangewebservices\",texchangewebservices.count = 1 ON MATCH SET texchangewebservices.count = texchangewebservices.count+1  MERGE (tpythonn_3n_x:Tag{tag:'python-3.x'}) ON CREATE SET tpythonn_3n_x.tag = \"python-3.x\",tpythonn_3n_x.count = 1 ON MATCH SET tpythonn_3n_x.count = tpythonn_3n_x.count+1  MERGE (u10695702:User{userId:10695702}) ON CREATE SET u10695702.name = \"Anchal Gupta\",u10695702.acceptRate = null,u10695702.reputation = 46 ON MATCH SET u10695702.acceptRate = null,u10695702.reputation = 46  MERGE (u10337562:User{userId:10337562}) ON CREATE SET u10337562.name = \"Victor Marcus\",u10337562.acceptRate = null,u10337562.reputation = 1 ON MATCH SET u10337562.acceptRate = null,u10337562.reputation = 1  MERGE (tjava)-[tjavatexchangen_server:Connected]-(texchangen_server)  ON CREATE SET tjavatexchangen_server.count = 1 ON MATCH SET tjavatexchangen_server.count = tjavatexchangen_server.count+1  MERGE (tjava)-[tjavatexchangewebservices:Connected]-(texchangewebservices)  ON CREATE SET tjavatexchangewebservices.count = 1 ON MATCH SET tjavatexchangewebservices.count = tjavatexchangewebservices.count+1  MERGE (tpython)-[tpythonu10695702:Knows]-(u10695702)  ON CREATE SET tpythonu10695702.count = 1 ON MATCH SET tpythonu10695702.count = tpythonu10695702.count+1  MERGE (ttensorflow)-[ttensorflowu10695702:Knows]-(u10695702)  ON CREATE SET ttensorflowu10695702.count = 1 ON MATCH SET ttensorflowu10695702.count = ttensorflowu10695702.count+1  MERGE (tpythonn_3n_x)-[tpythonn_3n_xu10695702:Knows]-(u10695702)  ON CREATE SET tpythonn_3n_xu10695702.count = 1 ON MATCH SET tpythonn_3n_xu10695702.count = tpythonn_3n_xu10695702.count+1  MERGE (tjava)-[tjavau10337562:Knows]-(u10337562)  ON CREATE SET tjavau10337562.count = 1 ON MATCH SET tjavau10337562.count = tjavau10337562.count+1  MERGE (texchangen_server)-[texchangen_serveru10337562:Knows]-(u10337562)  ON CREATE SET texchangen_serveru10337562.count = 1 ON MATCH SET texchangen_serveru10337562.count = texchangen_serveru10337562.count+1  MERGE (texchangewebservices)-[texchangewebservicesu10337562:Knows]-(u10337562)  ON CREATE SET texchangewebservicesu10337562.count = 1 ON MATCH SET texchangewebservicesu10337562.count = texchangewebservicesu10337562.count+1";
        String query = mapper.map(response);
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void mapJsonToEmptyQueryTest() {
        String json = "{\"items\":[],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        GraphModelMapper mapper = new GraphModelMapper();
        AnswerResponse response = new StringToAnswerMapper().map(json);
        String expectedQuery = "match (n:Node) return 1";
        String query = mapper.map(response);
        Assert.assertEquals(expectedQuery, query);
    }
}