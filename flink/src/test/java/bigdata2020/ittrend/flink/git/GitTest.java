package bigdata2020.ittrend.flink.git;

import bigdata2020.ittrend.flink.git.model.CleanRepo;
import bigdata2020.ittrend.flink.git.model.Neo4jBatch;
import bigdata2020.ittrend.flink.git.model.json.Contributor;
import bigdata2020.ittrend.flink.git.model.json.GitData;
import bigdata2020.ittrend.flink.git.model.neo4j.ContributorNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicRelation;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicUserRelation;
import bigdata2020.ittrend.flink.git.proc.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class GitTest {

    ParseGitDataJsonMapper jsonMapper = new ParseGitDataJsonMapper();
    JoinRepoMapper joinMapper = new JoinRepoMapper();
    GitGraphMapper graphMapper = new GitGraphMapper();
    GitQueryMapper queryMapper = new GitQueryMapper();

    private String read() {
        File file = new File("./github_merged.json");
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            return new String(data, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void test() {
        String str = read();
        System.out.println(str.length());
        List<String> queries = process(str);
        int tooBig = 0;

        int length = queries.stream().mapToInt(q -> q.length()).sum();
        System.out.println(length);

        for (String query : queries.stream().limit(20).collect(Collectors.toList())) {
            System.out.println(query);
        }


        System.out.println(queries.stream().mapToInt(q -> q.length()).max());
        System.out.println(queries.size());

        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"nodejs/node-v0.x-archive\", \"topics\": {\"names\": []}}, {\"repo_name\": \"karpathy/neuraltalk2\", \"topics\": {\"names\": []}}, {\"repo_name\": \"Mango/slideout\", \"topics\": {\"names\": [\"slideout-menu\", \"touch-events\", \"sidebar\", \"offcanvas\", \"menu-navigation\"]}}, {\"repo_name\": \"SheetJS/js-xlsx\", \"topics\": {\"names\": [\"xlsx\", \"xls\", \"excel\", \"ods\", \"spreadsheet\", \"data\", \"xml\", \"csv\", \"database\", \"nodejs\", \"html5\", \"javascript\", \"ios\", \"json\", \"react\", \"vue\", \"angular\", \"dbf\", \"table\", \"html\"]}}, {\"repo_name\": \"gradle/gradle\", \"topics\": {\"names\": [\"gradle\", \"build-tool\"]}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"drybjed\", \"contributions\": 14}, {\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"ganto\", \"contributions\": 11}, {\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"scibi\", \"contributions\": 3}, {\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"carlalexander\", \"contributions\": 2}, {\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"prahal\", \"contributions\": 1}]\n" +
                "}";

    }

    public void test2() {

        try {
            String str = read();
            System.out.println(str.length());
            GitData gitData = jsonMapper.map(str);
            List<CleanRepo> repoList = joinMapper.toList(gitData);

            Map<String, Long> counted = repoList.stream().flatMap(repo -> repo.getTopics().stream())
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            TopicNode topicNode;

            List<TopicNode> topics = counted.entrySet().stream()
                    .map(kvp -> TopicNode.builder().tag(kvp.getKey()).gitCount(kvp.getValue()).build())
                    .collect(Collectors.toList());

            List<String> queries = topics.stream().map(GitQueryUtils2::prepareTopicNode).collect(Collectors.toList());


            int pageSize = 50;

            List<String> merged = IntStream.range(0, (queries.size() + pageSize - 1) / pageSize)
                    .mapToObj(i -> queries.subList(i * pageSize, Math.min(pageSize * (i + 1), queries.size())))
                    .map(list -> String.join("", list))
                    .collect(Collectors.toList());

            System.out.println(merged.size());
            System.out.println(merged.stream().mapToInt(String::length).max());

            for (String query : merged.stream().limit(5).collect(Collectors.toList())) {
                System.out.println(query);
            }

            String result = String.join("#####", merged);

            File file2 = new File("./git_topic_node_queries");
            FileOutputStream fos = new FileOutputStream(file2);
            fos.write(result.getBytes(StandardCharsets.UTF_8));
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void test3() throws IOException {

        String str = read();
        System.out.println(str.length());
        GitData gitData = jsonMapper.map(str);
        List<CleanRepo> repoList = joinMapper.toList(gitData);
        List<Neo4jBatch> batches = repoList.stream().map(repo -> graphMapper.map(repo)).collect(Collectors.toList());

        Map<String, List<String>> topicRelations = batches.stream().flatMap(batch -> batch.getTopics().stream())
                .collect(
                        Collectors.groupingBy(
                                TopicNode::getTag,
                                Collectors.reducing(
                                        new ArrayList<>(),
                                        (TopicNode topic) -> topic.getConnected().values().stream().map(TopicRelation::getTo).collect(Collectors.toList()),
                                        (acc, list) -> Stream.concat(acc.stream(), list.stream()).collect(Collectors.toList())
                                )
                        )
                );

        List<TopicRelation3> counted = topicRelations.entrySet().stream()
                .map(kvp -> TopicRelation2.builder()
                        .name(kvp.getKey())
                        .to(kvp.getValue()
                                .stream()
                                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                        )
                        .build()
                )
                .map(topic -> TopicRelation3.builder()
                        .name(topic.getName())
                        .to(topic.getTo().entrySet().stream().collect(Collectors.groupingBy(
                                (Map.Entry<String, Long> kvp) -> kvp.getValue(),
                                Collectors.mapping(
                                        (Map.Entry<String, Long> kvp) -> kvp.getKey(),
                                        Collectors.toList()
                                )
                        )))
                        .build()
                )
                .collect(Collectors.toList());


        int i = 0;
        List<String> queries = counted.stream().flatMap(topic -> {
            String from = GitQueryUtils2.matchFrom(topic.getName());

            return topic.to.entrySet().stream().map(entry -> {
                String to = GitQueryUtils2.matchTo(entry.getValue());
                String connected = GitQueryUtils2.connected(entry.getKey());
                return from + " " + to + " " + connected;
            });
        }).collect(Collectors.toList());



        System.out.println(queries.size());
        for (String query : queries.stream().limit(5).collect(Collectors.toList())) {
            System.out.println(query);
        }

        String result = String.join("#####", queries);

        System.out.println(result.length());

        File file2 = new File("./git_topic_relation_queries");
        FileOutputStream fos = new FileOutputStream(file2);
        fos.write(result.getBytes(StandardCharsets.UTF_8));
        fos.close();
    }

    public void test4() {


        String str = read();
        System.out.println(str.length());
        GitData gitData = jsonMapper.map(str);
        List<CleanRepo> repoList = joinMapper.toList(gitData);

        Map<String, Long> counted = repoList.stream().flatMap(repo -> repo.getContributors() == null ? Stream.empty() : repo.getContributors().stream())
                .map(c -> c.getLogin())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(counted.size());
    }

    private List<String> process(String json) {
        GitData gitData = jsonMapper.map(json);
        List<CleanRepo> repoList = joinMapper.toList(gitData);
        List<Neo4jBatch> batchList = repoList.stream().map(repo -> graphMapper.map(repo)).collect(Collectors.toList());
        List<String> queries = batchList.stream().flatMap(batch -> queryMapper.toList(batch).stream()).collect(Collectors.toList());
        return queries;
    }

    public void test5() throws IOException {
        String str = read();
        System.out.println(str.length());
        GitData gitData = jsonMapper.map(str);
        List<CleanRepo> repoList = joinMapper.toList(gitData);

        Map<String, IntSummaryStatistics> contributions = repoList.stream()
                .flatMap(repo -> repo.getContributors() == null ? Stream.empty() : repo.getContributors().stream())
                .collect(Collectors.groupingBy(Contributor::getLogin, Collectors.summarizingInt(Contributor::getContributions)));

        List<ContributorNode> contributors = contributions.entrySet().stream()
                .map(kvp -> ContributorNode.builder().displayName(kvp.getKey()).contributions((int)kvp.getValue().getSum()).build())
                .collect(Collectors.toList());

        List<String> queries = contributors.stream().map(GitQueryUtils2::prepareContributorNode).collect(Collectors.toList());

        System.out.println(queries.size());

        int pageSize = 50;

        List<String> merged = IntStream.range(0, (queries.size() + pageSize - 1) / pageSize)
                .mapToObj(i -> queries.subList(i * pageSize, Math.min(pageSize * (i + 1), queries.size())))
                .map(list -> String.join("", list))
                .collect(Collectors.toList());

        System.out.println(merged.size());
        System.out.println(merged.stream().mapToInt(String::length).max());

        for (String query : merged.stream().limit(5).collect(Collectors.toList())) {
            System.out.println(query);
        }

        String result = String.join("#####", merged);


            File file2 = new File("./git_contributor_node_queries");
            FileOutputStream fos = new FileOutputStream(file2);
            fos.write(result.getBytes(StandardCharsets.UTF_8));
            fos.close();


    }

    @Test
    public void test6() throws IOException {
        String str = read();
        System.out.println(str.length());
        GitData gitData = jsonMapper.map(str);
        List<CleanRepo> repoList = joinMapper.toList(gitData);
        List<Neo4jBatch> batches = repoList.stream().map(repo -> graphMapper.map(repo)).collect(Collectors.toList());

        Map<String, List<String>> contributorRelations = batches.stream().flatMap(batch -> batch.getContributors().stream())
                .collect(
                        Collectors.groupingBy(
                                ContributorNode::getDisplayName,
                                Collectors.reducing(
                                        new ArrayList<>(),
                                        (ContributorNode contributor) -> contributor.getTags().values().stream().map(TopicUserRelation::getTag).collect(Collectors.toList()),
                                        (acc, list) -> Stream.concat(acc.stream(), list.stream()).collect(Collectors.toList())
                                )
                        )
                );

        List<TopicRelation3> counted = contributorRelations.entrySet().stream()
                .map(kvp -> TopicRelation2.builder()
                        .name(kvp.getKey())
                        .to(kvp.getValue()
                                .stream()
                                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                        )
                        .build()
                )
                .map(topic -> TopicRelation3.builder()
                        .name(topic.getName())
                        .to(topic.getTo().entrySet().stream().collect(Collectors.groupingBy(
                                (Map.Entry<String, Long> kvp) -> kvp.getValue(),
                                Collectors.mapping(
                                        (Map.Entry<String, Long> kvp) -> kvp.getKey(),
                                        Collectors.toList()
                                )
                        )))
                        .build()
                )
                .collect(Collectors.toList());

        List<String> queries = counted.stream().flatMap(topic -> {
            String from = GitQueryUtils2.matchUser(topic.getName());

            return topic.to.entrySet().stream().map(entry -> {
                String to = GitQueryUtils2.matchTag(entry.getValue());
                String connected = GitQueryUtils2.knows(entry.getKey());
                return from + " " + to + " " + connected;
            });
        }).collect(Collectors.toList());



        System.out.println(queries.size());
        for (String query : queries.stream().limit(5).collect(Collectors.toList())) {
            System.out.println(query);
        }

        int pageSize = 5000;

        List<String> merged = IntStream.range(0, (queries.size() + pageSize - 1) / pageSize)
                .mapToObj(i -> queries.subList(i * pageSize, Math.min(pageSize * (i + 1), queries.size())))
                .map(list -> String.join("#####", list))
                .collect(Collectors.toList());

//        String result = String.join("#####", queries);

        System.out.println(merged.size());

        for (int i = 0; i < merged.size(); i++) {
            File file2 = new File("./git_rel/git_contributor_relation_queries_" + i);
            FileOutputStream fos = new FileOutputStream(file2);
            fos.write(merged.get(i).getBytes(StandardCharsets.UTF_8));
            fos.close();
        }

//        System.out.println(result.length());
//
//        File file2 = new File("./git_contributor_relation_queries");
//        FileOutputStream fos = new FileOutputStream(file2);
//        fos.write(result.getBytes(StandardCharsets.UTF_8));
//        fos.close();

    }


    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class TopicRelation2 {
        private String name;
        private Map<String, Long> to;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class TopicRelation3 {
        private String name;
        private Map<Long, List<String>> to;
    }
}
