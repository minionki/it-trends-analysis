package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.CleanRepo;
import bigdata2020.ittrend.flink.git.model.Neo4jBatch;
import bigdata2020.ittrend.flink.git.model.json.GitData;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class GitQueryUtilsTest {

    @Test
    public void matchTagQueryTest() {
        Neo4jBatch neo4jBatch = prepareDefaultData();

        String query = GitQueryUtils.matchTags(neo4jBatch.getTopics());
        String expectedQuery = "MATCH (tag:Tag) where tag.tag in ['javascript','css','html']";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void matchUserQueryTest() {
        Neo4jBatch neo4jBatch = prepareDefaultData();

        String query = GitQueryUtils.matchUsers(neo4jBatch.getContributors());
        String expectedQuery = "MATCH (user:User) where user.name in ['kowalski']";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void mergeTagQueryTest() {
        Neo4jBatch neo4jBatch = prepareDefaultData();

        String query = GitQueryUtils.mergeTagList(neo4jBatch.getTopics());
        System.out.println(query);
        String expectedQuery = " MERGE (tjavascript:Tag{tag:'javascript'}) ON CREATE SET tjavascript.gitCount = 1 ON MATCH SET tjavascript.gitCount = tjavascript.gitCount+1  MERGE (tcss:Tag{tag:'css'}) ON CREATE SET tcss.gitCount = 1 ON MATCH SET tcss.gitCount = tcss.gitCount+1  MERGE (thtml:Tag{tag:'html'}) ON CREATE SET thtml.gitCount = 1 ON MATCH SET thtml.gitCount = thtml.gitCount+1";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void mergeUserQueryTest() {
        Neo4jBatch neo4jBatch = prepareDefaultData();

        String query = GitQueryUtils.mergeUserList(neo4jBatch.getContributors());
        System.out.println(query);
        String expectedQuery = " MERGE (uukowalski:User{name:'kowalski'}) ON CREATE SET uukowalski.name = \"kowalski\",uukowalski.contributions = 10 ON MATCH SET uukowalski.contributions = uukowalski.contributions+10";
        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void createRelationshipsTest() {
        Neo4jBatch neo4jBatch = prepareDefaultData();

        String query = GitQueryUtils.createConnections(neo4jBatch.getTopics(), neo4jBatch.getContributors());
        System.out.println(query);
        String expectedQuery = "MATCH (tag:Tag) where tag.tag in ['javascript','css','html'] MATCH (tag2:Tag) where tag.tag in ['javascript','css','html'] MATCH (user:User) where user.name in ['kowalski']  MERGE (tag)-[connected:Connected]-(tag2)  ON CREATE SET connected.gitCount = 1 ON MATCH SET connected.gitCount = connected.gitCount+1 MERGE (user)-[knows:Knows]-(tag)  ON CREATE SET knows.gitCount = 1 ON MATCH SET knows.gitCount = knows.gitCount+1";
        Assert.assertEquals(expectedQuery, query);
    }

    private Neo4jBatch prepareDefaultData() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"front\", \"topics\": {\"names\": [\"javascript\", \"css\", \"html\"]}}, {\"repo_name\": \"back\", \"topics\": {\"names\": [\"c#\", \".net\"]}}, {\"repo_name\": \"other\", \"topics\": {\"names\": [\"topic\"]}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"front\", \"login\": \"kowalski\", \"contributions\": 10}, {\"repo_name\": \"back\", \"login\": \"nowak\", \"contributions\": 1}, {\"repo_name\": \"back\", \"login\": \"john\", \"contributions\": 10}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);
        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);
        CleanRepo repo = repoList.get(0);

        return new GitGraphMapper().map(repo);
    }

}