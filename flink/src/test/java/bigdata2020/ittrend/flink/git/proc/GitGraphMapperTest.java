package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.CleanRepo;
import bigdata2020.ittrend.flink.git.model.Neo4jBatch;
import bigdata2020.ittrend.flink.git.model.json.GitData;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class GitGraphMapperTest {


    @Test
    public void gitGraphMapperTest() {
        String json = "    {\n" +
                "        \"repos\": [{\"repo_name\": \"back\", \"topics\": {\"names\": [\"c#\", \".net\"]}}],\n" +
                "        \"contributors\": [{\"repo_name\": \"back\", \"login\": \"nowak\", \"contributions\": 1}, {\"repo_name\": \"back\", \"login\": \"john\", \"contributions\": 10}]\n" +
                "    }";

        GitData gitData = new ParseGitDataJsonMapper().map(json);
        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);
        CleanRepo repo = repoList.get(0);

        Neo4jBatch neo4jBatch = new GitGraphMapper().map(repo);

        Assert.assertEquals(2, neo4jBatch.getTopics().size());
        Assert.assertEquals(2, neo4jBatch.getContributors().size());

        Assert.assertEquals(1, neo4jBatch.getTopics().get(0).getConnected().size());
        Assert.assertEquals("c#", neo4jBatch.getTopics().get(0).getConnected().get(".net").getFrom());
        Assert.assertEquals(".net", neo4jBatch.getTopics().get(0).getConnected().get(".net").getTo());
        Assert.assertEquals(1, neo4jBatch.getTopics().get(0).getConnected().get(".net").getGitCount());

        Assert.assertEquals(2, neo4jBatch.getContributors().get(0).getTags().size());
        Assert.assertEquals("nowak", neo4jBatch.getContributors().get(0).getDisplayName());
        Assert.assertEquals("c#", neo4jBatch.getContributors().get(0).getTags().get("c#").getTag());
        Assert.assertEquals(1, neo4jBatch.getContributors().get(0).getTags().get("c#").getGitCount());
    }

    @Test
    public void gitGraphMapperTestMissingContributors() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"back\", \"topics\": {\"names\": [\"c#\", \".net\"]}}],\n" +
                "  \"contributors\": []\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);
        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);
        CleanRepo repo = repoList.get(0);

        Neo4jBatch neo4jBatch = new GitGraphMapper().map(repo);

        Assert.assertEquals(2, neo4jBatch.getTopics().size());
        Assert.assertEquals(0, neo4jBatch.getContributors().size());

        Assert.assertEquals(1, neo4jBatch.getTopics().get(0).getConnected().size());
        Assert.assertEquals("c#", neo4jBatch.getTopics().get(0).getConnected().get(".net").getFrom());
        Assert.assertEquals(".net", neo4jBatch.getTopics().get(0).getConnected().get(".net").getTo());
        Assert.assertEquals(1, neo4jBatch.getTopics().get(0).getConnected().get(".net").getGitCount());
    }

    @Test
    public void gitGraphMapperTestMissingTopics() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"back\", \"topics\": {\"names\": []}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"back\", \"login\": \"nowak\", \"contributions\": 1}, {\"repo_name\": \"back\", \"login\": \"john\", \"contributions\": 10}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);
        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);
        CleanRepo repo = repoList.get(0);

        Neo4jBatch neo4jBatch = new GitGraphMapper().map(repo);

        Assert.assertEquals(0, neo4jBatch.getTopics().size());
        Assert.assertEquals(2, neo4jBatch.getContributors().size());

        Assert.assertEquals(0, neo4jBatch.getContributors().get(0).getTags().size());
        Assert.assertEquals(0, neo4jBatch.getContributors().get(1).getTags().size());
    }

}