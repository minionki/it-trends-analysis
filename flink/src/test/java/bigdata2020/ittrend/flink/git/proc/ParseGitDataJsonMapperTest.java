package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.json.GitData;
import org.junit.Assert;
import org.junit.Test;

public class ParseGitDataJsonMapperTest {

    @Test
    public void parseGitJson() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"SheetJS/js-xlsx\", \"topics\": {\"names\": [\"xlsx\", \"xls\", \"excel\", \"ods\", \"spreadsheet\", \"data\", \"xml\", \"csv\", \"database\", \"nodejs\", \"html5\", \"javascript\", \"ios\", \"json\", \"react\", \"vue\", \"angular\", \"dbf\", \"table\", \"html\"]}}, {\"repo_name\": \"gradle/gradle\", \"topics\": {\"names\": [\"gradle\", \"build-tool\"]}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"carlalexander\", \"contributions\": 2}, {\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"prahal\", \"contributions\": 1}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);

        Assert.assertEquals(2, gitData.getRepos().size());
        Assert.assertEquals(2, gitData.getContributors().size());

        Assert.assertEquals("gradle/gradle", gitData.getRepos().get(1).getRepoName());
        Assert.assertEquals(2, gitData.getRepos().get(1).getTopics().getNames().size());
        Assert.assertEquals("gradle", gitData.getRepos().get(1).getTopics().getNames().get(0));
        Assert.assertEquals("build-tool", gitData.getRepos().get(1).getTopics().getNames().get(1));

        Assert.assertEquals(2, gitData.getContributors().get(0).getContributions());
        Assert.assertEquals("carlalexander", gitData.getContributors().get(0).getLogin());
        Assert.assertEquals("debops/ansible-fail2ban", gitData.getContributors().get(0).getRepoName());
    }

    @Test
    public void parseGitJsonEmptyRepos() {
        String json = "{\n" +
                "  \"repos\": [],\n" +
                "  \"contributors\": [{\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"carlalexander\", \"contributions\": 2}, {\"repo_name\": \"debops/ansible-fail2ban\", \"login\": \"prahal\", \"contributions\": 1}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);

        Assert.assertEquals(0, gitData.getRepos().size());
        Assert.assertEquals(2, gitData.getContributors().size());
    }

    @Test
    public void parseGitJsonEmptyContributors() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"SheetJS/js-xlsx\", \"topics\": {\"names\": [\"xlsx\", \"xls\", \"excel\", \"ods\", \"spreadsheet\", \"data\", \"xml\", \"csv\", \"database\", \"nodejs\", \"html5\", \"javascript\", \"ios\", \"json\", \"react\", \"vue\", \"angular\", \"dbf\", \"table\", \"html\"]}}, {\"repo_name\": \"gradle/gradle\", \"topics\": {\"names\": [\"gradle\", \"build-tool\"]}}],\n" +
                "  \"contributors\": []\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);

        Assert.assertEquals(2, gitData.getRepos().size());
        Assert.assertEquals(0, gitData.getContributors().size());
    }

}