package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.CleanRepo;
import bigdata2020.ittrend.flink.git.model.json.GitData;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class JoinRepoMapperTest {

    @Test
    public void joinRepoTest() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"front\", \"topics\": {\"names\": [\"javascript\", \"css\", \"html\"]}}, {\"repo_name\": \"back\", \"topics\": {\"names\": [\"c#\", \".net\"]}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"front\", \"login\": \"kowalski\", \"contributions\": 10}, {\"repo_name\": \"back\", \"login\": \"nowak\", \"contributions\": 1}, {\"repo_name\": \"back\", \"login\": \"john\", \"contributions\": 10}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);

        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);

        Assert.assertEquals(2, repoList.size());

        Assert.assertEquals("front", repoList.get(0).getRepoName());
        Assert.assertEquals(3, repoList.get(0).getTopics().size());
        Assert.assertEquals(2, repoList.get(1).getContributors().size());
    }

    @Test
    public void joinRepoTestMissingContributors() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"front\", \"topics\": {\"names\": [\"javascript\", \"css\", \"html\"]}}, {\"repo_name\": \"back\", \"topics\": {\"names\": [\"c#\", \".net\"]}}, {\"repo_name\": \"other\", \"topics\": {\"names\": [\"topic\"]}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"front\", \"login\": \"kowalski\", \"contributions\": 10}, {\"repo_name\": \"back\", \"login\": \"nowak\", \"contributions\": 1}, {\"repo_name\": \"back\", \"login\": \"john\", \"contributions\": 10}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);

        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);

        Assert.assertEquals(3, repoList.size());

        Assert.assertEquals("other", repoList.get(2).getRepoName());
        Assert.assertEquals(1, repoList.get(2).getTopics().size());
        assertNull(repoList.get(2).getContributors());
    }

    @Test
    public void joinRepoTestMissingRepos() {
        String json = "{\n" +
                "  \"repos\": [{\"repo_name\": \"front\", \"topics\": {\"names\": [\"javascript\", \"css\", \"html\"]}}],\n" +
                "  \"contributors\": [{\"repo_name\": \"front\", \"login\": \"kowalski\", \"contributions\": 10}, {\"repo_name\": \"back\", \"login\": \"nowak\", \"contributions\": 1}, {\"repo_name\": \"back\", \"login\": \"john\", \"contributions\": 10}]\n" +
                "}";

        GitData gitData = new ParseGitDataJsonMapper().map(json);

        List<CleanRepo> repoList = new JoinRepoMapper().toList(gitData);

        Assert.assertEquals(1, repoList.size());
        Assert.assertEquals("front", repoList.get(0).getRepoName());
    }

}