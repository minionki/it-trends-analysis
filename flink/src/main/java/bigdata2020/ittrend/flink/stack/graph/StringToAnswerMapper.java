package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import bigdata2020.ittrend.flink.stack.model.json.QuestionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.api.common.functions.MapFunction;

import java.io.IOException;

public class StringToAnswerMapper implements MapFunction<String, AnswerResponse> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public AnswerResponse map(String json) {
        try {
            return mapper.readValue(json, AnswerResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
