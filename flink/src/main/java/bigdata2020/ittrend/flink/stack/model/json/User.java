package bigdata2020.ittrend.flink.stack.model.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @JsonProperty("accept_rate")
    private Long acceptRate;

    @JsonProperty("display_name")
    private String displayName;

    private Long reputation;

    @JsonProperty("user_id")
    private long userId;

}
