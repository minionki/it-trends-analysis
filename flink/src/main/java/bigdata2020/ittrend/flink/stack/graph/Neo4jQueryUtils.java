package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.neo4j.*;
import bigdata2020.ittrend.flink.stack.util.Neo4jQueryUtilsOld;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collection;
import java.util.stream.Collectors;

public class Neo4jQueryUtils {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
    }

    public static String mergeTagList(Collection<TagNode> tagList) {
        return tagList.stream()
                .map(Neo4jQueryUtils::prepareTagNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeUserList(Collection<UserNode> userList) {
        return userList.stream()
                .map(Neo4jQueryUtilsOld::prepareUserNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeTagRelations(Collection<TagNode> tagList) {
        return tagList.stream()
                .flatMap(tag -> tag.getConnected().values().stream())
                .map(Neo4jQueryUtils::prepareTagRelationship)
                .collect(Collectors.joining(" "));
    }

    public static String mergeUserTagRelations(Collection<UserNode> userList) {
        return userList.stream()
                .flatMap(user -> user.getTags().values().stream())
                .map(Neo4jQueryUtils::prepareUserTagRelation)
                .collect(Collectors.joining(" "));
    }

    private static String prepareTagNode(TagNode tag) {
        String label = tagLabel(tag.getTag());

        return " MERGE (" + label + ":Tag" + "{tag:\'" + tag.getTag() + "\'})"
                + " ON CREATE SET "
                + setStringProperty(label, "tag", tag.getTag()) + ","
                + setProperty(label, "count", tag.getCount())
                + " ON MATCH SET "
                + addToProperty(label, "count", tag.getCount());
    }

    private static String prepareTagRelationship(TagRelation tagRelation) {
        String fromTag = tagLabel(tagRelation.getFrom());
        String toTag = tagLabel(tagRelation.getTo());
        String relLabel = fromTag + toTag;

        return " MERGE (" + fromTag + ")" +
                "-[" + relLabel + ":Connected]-" +
                "(" + toTag + ") "
                + " ON CREATE SET " + setProperty(relLabel, "count", tagRelation.getCount())
                + " ON MATCH SET " + addToProperty(relLabel, "count", tagRelation.getCount());
    }

    public static String prepareUserTagRelation(UserTagRelation userTagRelation) {
        String tagLabel = tagLabel(userTagRelation.getTag());
        String userLabel = "u" + userTagRelation.getUserId();
        String relLabel = tagLabel + userLabel;
        return " MERGE (" + tagLabel + ")" +
                "-[" + relLabel + ":Knows]-" +
                "(" + userLabel + ") "
                + " ON CREATE SET " + setProperty(relLabel, "count", userTagRelation.getCount())
                + " ON MATCH SET " + addToProperty(relLabel, "count", userTagRelation.getCount());
    }

    public static String prepareUserNode(UserNode user) {
        String label = "u" + user.getUserId();
        return " MERGE (" + label + ":User" + "{userId:" + user.getUserId() + "})"
                + " ON CREATE SET "
                + setStringProperty(label, "name", user.getDisplayName()) + ","
                + setProperty(label, "acceptRate", user.getAcceptRate()) + ","
                + setProperty(label, "reputation", user.getReputation())
                + " ON MATCH SET "
                + setProperty(label, "acceptRate", user.getAcceptRate()) + ","
                + setProperty(label, "reputation", user.getReputation());
    }

    private static String tagLabel(String tag) {
        return "t" + tag.replaceAll("[^a-zA-Z0-9]", "n_");
    }


    private static String setProperty(String label, String property, Object value) {
        return label + "." + property + " = " + value;
    }

    private static String addToProperty(String label, String property, Object value) {
        return label + "." + property + " = " + label + "." + property + "+" + value;
    }


    private static String setStringProperty(String label, String property, Object value) {
        return label + "." + property + " = \"" + value + "\"";
    }
}
