package bigdata2020.ittrend.flink.stack.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserNode {

    private Long acceptRate;

    private String displayName;

    private Long reputation;

    private long userId;

    private Map<String, UserTagRelation> tags;

}
