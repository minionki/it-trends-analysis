package bigdata2020.ittrend.flink.stack.util;

import bigdata2020.ittrend.flink.stack.model.json.Question;
import bigdata2020.ittrend.flink.stack.model.json.QuestionResponse;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;

public class QuestionResponseSplitter implements FlatMapFunction<QuestionResponse, QuestionResponse> {

    @Override
    public void flatMap(QuestionResponse questionResponse, Collector<QuestionResponse> collector) {
        List<Question> items = questionResponse.getItems();

        for (int from = 0; from < items.size(); from += 5) {
            int to = from + 10;
            if (to > items.size()) {
                to = items.size();
            }
            collector.collect(QuestionResponse.builder().items(new ArrayList<>(items.subList(from, to))).build());
        }
    }
}
