package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacket;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ParseModelMapper implements MapFunction<NiFiDataPacket, AnswerResponse> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public AnswerResponse map(NiFiDataPacket niFiDataPacket) {
        String content = new String(niFiDataPacket.getContent(), StandardCharsets.UTF_8);
        try {
            return mapper.readValue(content, AnswerResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
