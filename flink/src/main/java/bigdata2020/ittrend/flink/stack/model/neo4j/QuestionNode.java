package bigdata2020.ittrend.flink.stack.model.neo4j;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionNode {

    @JsonIgnore
    private List<String> tags;

    @JsonIgnore
    private Map<Long, UserNode> users;

    private Long activityCount;
    private Long questionId;
}
