package bigdata2020.ittrend.flink.stack.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagNode {

    private String tag;
    private long count = 0L;
    private Map<String, TagRelation> connected;
}
