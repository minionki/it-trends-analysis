package bigdata2020.ittrend.flink.stack.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagRelation {
    private String from;
    private String to;
    private long count = 0L;
}
