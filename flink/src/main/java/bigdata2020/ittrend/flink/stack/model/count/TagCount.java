package bigdata2020.ittrend.flink.stack.model.count;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagCount {
    private String name;
    private int count;
}
