package bigdata2020.ittrend.flink.stack.stream;

import bigdata2020.ittrend.flink.EnvironmentFactory;
import bigdata2020.ittrend.flink.stack.count.HttpCallMapper;
import bigdata2020.ittrend.flink.stack.count.TagCounterMapper;
import bigdata2020.ittrend.flink.stack.graph.ParseModelMapper;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacket;

public class StackTagCounter {
    private static final String SOURCE_PORT_NAME = "StackTagForFlink";

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment environment = EnvironmentFactory.defaultStreamEnvironment();

        SourceFunction<NiFiDataPacket> nifiSource = EnvironmentFactory.nifiSource(SOURCE_PORT_NAME);

        environment
                .addSource(nifiSource)
                .map(new ParseModelMapper())
                .map(new TagCounterMapper())
                .map(new HttpCallMapper())
                .print();

        // execute program
        environment.execute("Tag Counter");
    }

}
