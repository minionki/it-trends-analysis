package bigdata2020.ittrend.flink.stack.graph;

import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import bigdata2020.ittrend.flink.stack.model.json.User;
import bigdata2020.ittrend.flink.stack.model.neo4j.TagNode;
import bigdata2020.ittrend.flink.stack.model.neo4j.TagRelation;
import bigdata2020.ittrend.flink.stack.model.neo4j.UserNode;
import bigdata2020.ittrend.flink.stack.model.neo4j.UserTagRelation;
import org.apache.flink.api.common.functions.MapFunction;

import java.util.HashMap;
import java.util.Map;

public class GraphModelMapper implements MapFunction<AnswerResponse, String> {

    public static void main(String[] args) {
        String xd = "{\"items\":[{\"tags\":[\"java\",\"exchange-server\",\"exchangewebservices\"],\"owner\":{\"reputation\":1,\"user_id\":10337562,\"display_name\":\"Victor Marcus\"},\"last_activity_date\":1587060858,\"creation_date\":1587060858,\"answer_id\":61257140},{\"tags\":[\"python\",\"python-3.x\",\"tensorflow\"],\"owner\":{\"reputation\":46,\"user_id\":10695702,\"display_name\":\"Anchal Gupta\"},\"last_activity_date\":1587060846,\"creation_date\":1587060846,\"answer_id\":61257135},{\"tags\":[\"reactjs\",\"firebase\",\"google-cloud-firestore\",\"react-props\"],\"owner\":{\"reputation\":6019,\"user_id\":2054602,\"display_name\":\"Radu Diță\"},\"last_activity_date\":1587060844,\"creation_date\":1587060844,\"answer_id\":61257134},{\"tags\":[\"php\",\"mongodb\",\"mongodb-query\",\"nosql\"],\"owner\":{\"reputation\":1682,\"user_id\":5065577,\"accept_rate\":64,\"display_name\":\"Bogdan  Dubyk\"},\"last_activity_date\":1587060842,\"creation_date\":1587060842,\"answer_id\":61257132},{\"tags\":[\"vuetify.js\"],\"owner\":{\"reputation\":51,\"user_id\":1287107,\"display_name\":\"Push\"},\"last_activity_date\":1587060834,\"creation_date\":1587060834,\"answer_id\":61257131},{\"tags\":[\"excel\",\"vba\",\"api\"],\"owner\":{\"reputation\":1,\"user_id\":13333159,\"display_name\":\"user13333159\"},\"last_activity_date\":1587060829,\"creation_date\":1587060829,\"answer_id\":61257128},{\"tags\":[\"c#\",\"rest\",\"console\",\"shopify\"],\"owner\":{\"reputation\":116,\"user_id\":11351008,\"display_name\":\"BenceL\"},\"last_activity_date\":1587060825,\"creation_date\":1587060825,\"answer_id\":61257126},{\"tags\":[\"java\",\"filesystems\",\"state\",\"apache-flink\",\"checkpoint\"],\"owner\":{\"reputation\":13223,\"user_id\":2000823,\"display_name\":\"David Anderson\"},\"last_activity_date\":1587060816,\"creation_date\":1587060816,\"answer_id\":61257124},{\"tags\":[\"python\",\"pandas\",\"datetime\",\"merge\"],\"owner\":{\"reputation\":745,\"user_id\":6695793,\"display_name\":\"jcaliz\"},\"last_activity_date\":1587060809,\"creation_date\":1587060809,\"answer_id\":61257123},{\"tags\":[\"c\",\"sockets\",\"ubuntu\"],\"owner\":{\"reputation\":77,\"user_id\":3062582,\"display_name\":\"YanBir\"},\"last_activity_date\":1587060790,\"creation_date\":1587060790,\"answer_id\":61257117},{\"tags\":[\"groovy\",\"visual-studio-code\",\"jenkins-groovy\"],\"owner\":{\"reputation\":13060,\"user_id\":104085,\"accept_rate\":73,\"display_name\":\"uzay95\"},\"last_activity_date\":1587060782,\"creation_date\":1587060782,\"answer_id\":61257115}],\"has_more\":false,\"quota_max\":10000,\"quota_remaining\":9793}";
        AnswerResponse q = new StringToAnswerMapper().map(xd);
        String query = new GraphModelMapper().map(q);
        System.out.println(query);
    }


    @Override
    public String map(AnswerResponse answerResponse) {
        if (answerResponse != null) {
            try {
                String result = doMap(answerResponse);
                if (result.length() > 64 * 1024) {
                    return "match (n:Node) return 1";
                } else if (result.trim().isEmpty()) {
                    return "match (n:Node) return 1";
                } else {
                    return result;
                }
            } catch (Exception e) {
                return "match (n:Node) return 1"; // this is to avoid bottleneck for errors
            }
        }
        return "";
    }

    public String doMap(AnswerResponse answerResponse) {

        Map<Long, UserNode> users = new HashMap<>();

        answerResponse.getItems()
                .forEach(a -> {
                    User user = a.getOwner();
                    UserNode userNode = users.get(user.getUserId());

                    if (userNode == null) {
                        userNode = UserNode.builder()
                                .userId(user.getUserId())
                                .acceptRate(user.getAcceptRate())
                                .reputation(user.getReputation())
                                .displayName(user.getDisplayName())
                                .tags(new HashMap<>())
                                .build();
                    }

                    for (String tag : a.getTags()) {
                        UserTagRelation relation = userNode.getTags().get(tag);
                        if (relation == null) {
                            relation = UserTagRelation.builder()
                                    .tag(tag)
                                    .userId(user.getUserId())
                                    .build();
                        }
                        relation.setCount(relation.getCount() + 1);
                        userNode.getTags().put(tag, relation);
                    }

                    users.put(user.getUserId(), userNode);
                });

        Map<String, TagNode> tags = new HashMap<>();

        answerResponse.getItems().forEach(a -> {

            for (int from = 0; from < a.getTags().size(); from++) {
                String tag = a.getTags().get(from);
                TagNode tagNode = tags.get(tag);
                if (tagNode == null) {
                    tagNode = TagNode.builder().tag(tag).connected(new HashMap<>()).build();
                }
                tagNode.setCount(tagNode.getCount() + 1);
                tags.put(tag, tagNode);

                for (int to = from; to < a.getTags().size(); to++) {
                    String toTag = a.getTags().get(to);
                    if (tag.compareTo(toTag) > 0) {
                        TagRelation connectedTag = tagNode.getConnected().get(toTag);
                        if (connectedTag == null) {
                            connectedTag = TagRelation.builder().from(tag).to(toTag).build();
                        }
                        connectedTag.setCount(connectedTag.getCount() + 1);
                        tagNode.getConnected().put(toTag, connectedTag);
                    }
                }
            }
        });



        return Neo4jQueryUtils.mergeTagList(tags.values())
                + " "
                + Neo4jQueryUtils.mergeUserList(users.values())
                + " "
                + Neo4jQueryUtils.mergeTagRelations(tags.values())
                + " "
                + Neo4jQueryUtils.mergeUserTagRelations(users.values());
    }
}
