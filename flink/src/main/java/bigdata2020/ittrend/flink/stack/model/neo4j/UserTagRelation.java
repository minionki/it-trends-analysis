package bigdata2020.ittrend.flink.stack.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTagRelation {
    private Long userId;
    private String tag;
    private long count = 0L;
}
