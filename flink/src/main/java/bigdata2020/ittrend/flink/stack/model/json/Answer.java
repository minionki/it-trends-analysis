package bigdata2020.ittrend.flink.stack.model.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Answer {

    private List<String> tags;

    @JsonProperty("answer_id")
    private long answerId;

    @JsonProperty("creation_date")
    private long creationDate;

    private User owner;
}
