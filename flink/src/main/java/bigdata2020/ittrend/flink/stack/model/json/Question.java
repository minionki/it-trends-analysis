package bigdata2020.ittrend.flink.stack.model.json;

import bigdata2020.ittrend.flink.stack.model.neo4j.UserNode;
import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Question {

    private List<String> tags;

    private List<Answer> answers;

    private List<Comment> comments;

    private User owner;

    @JsonProperty("comment_count")
    private long commentCount;

    @JsonProperty("delete_vote_count")
    private long deleteVoteCount;

    @JsonProperty("reopen_vote_count")
    private long reopenVoteCount;

    @JsonProperty("close_vote_count")
    private long closeVoteCount;

    @JsonProperty("view_count")
    private long viewCount;

    @JsonProperty("favourite_count")
    private long favouriteCount;

    @JsonProperty("down_vote_count")
    private long downVoteCount;

    @JsonProperty("up_vote_count")
    private long upVoteCount;

    @JsonProperty("answer_count")
    private long answerCount;

    @JsonProperty("last_activity_date")
    private long lastActivityDate;

    @JsonProperty("question_id")
    private long questionId;

    private long score;

    @JsonIgnore
    public long getVoteCount() {
        return deleteVoteCount + reopenVoteCount + closeVoteCount + downVoteCount + upVoteCount + favouriteCount;
    }

    @JsonIgnore
    public long getReplyCount() {
        return commentCount + answerCount;
    }

    @JsonIgnore
    public Map<Long, UserNode> getUserNodes() {
        Map<Long, UserNode> users = new HashMap<>();
//        specialPut(users, owner, score);
//
//        if (answers != null) {
//            for (Answer answer : answers) {
//                specialPut(users, answer.getOwner(), answer.getScore());
//                if (answer.getComments() != null) {
//                    for (Comment comment : answer.getComments()) {
//                        specialPut(users, comment.getOwner(), comment.getScore());
//                    }
//                }
//            }
//        }
//        if (comments != null) {
//            for (Comment comment : comments) {
//                specialPut(users, comment.getOwner(), comment.getScore());
//            }
//        }
        return users;
    }

//    private void specialPut(Map<Long, UserNode> users, User user, long score) {
//        UserNode existing = users.get(user.getUserId());
//        if (existing != null) {
//            existing.setActivityScore(existing.getActivityScore() + score);
//        } else {
//            users.put(user.getUserId(), map(user, score));
//        }
//    }
//
//    private UserNode map(User user, long score) {
//        return UserNode.builder()
//                .reputation(user.getReputation())
//                .acceptRate(user.getAcceptRate())
//                .displayName(user.getDisplayName())
//                .userId(user.getUserId())
//                .activityScore(score)
//                .build();
//    }

    @JsonIgnore
    public long getAnswerVoteSum() {
//        if (answers != null){
//            return answers.stream()
//                    .collect(Collectors.summarizingLong(Answer::getVoteCount))
//                    .getSum();
//        } else {
//            return 0;
//        }
        return 0;
    }
}
