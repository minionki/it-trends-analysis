package bigdata2020.ittrend.flink.stack.util;

import bigdata2020.ittrend.flink.stack.model.neo4j.QuestionNode;
import bigdata2020.ittrend.flink.stack.model.neo4j.TagNode;
import bigdata2020.ittrend.flink.stack.model.neo4j.UserNode;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Neo4jQueryUtilsOld {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
    }

    public static String mergeQuestionListQuery(List<QuestionNode> questionList) {
        if (questionList.isEmpty()) {
            return "";
        }
        return questionList.stream()
                .map(Neo4jQueryUtilsOld::prepareQuestionNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeTagListQuery(List<TagNode> tagList) {
        if (tagList.isEmpty()) {
            return "";
        }
        return tagList.stream()
                .map(Neo4jQueryUtilsOld::prepareTagNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeTaggedRelationQuery(List<QuestionNode> questionList) {
        if (questionList.isEmpty()) {
            return "";
        }
        return questionList.stream()
                .map(Neo4jQueryUtilsOld::prepareRelations)
                .collect(Collectors.joining(" "));
    }

    public static String mergeUser(Collection<UserNode> userList) {
        return userList.stream()
                .map(Neo4jQueryUtilsOld::prepareUserNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeQuestionUser(List<QuestionNode> questionList) {
        return questionList.stream()
                .map(Neo4jQueryUtilsOld::prepareActivityRelations)
                .collect(Collectors.joining(" "));
    }

    private static String prepareTagNode(TagNode tag) {
        try {
            return " MERGE (t" + tag.getTag().replaceAll("[^a-zA-Z0-9]", "n_") + ":Tag" + MAPPER.writeValueAsString(tag) + ")";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String prepareQuestionNode(QuestionNode question) {
        String label = "q" + question.getQuestionId();
        return " MERGE (" + label + ":Question" + "{questionId:" + question.getQuestionId() + "})"
                + " ON CREATE SET " + setProperty(label, "activityCount", question.getActivityCount())
                + " ON MATCH SET " + setProperty(label, "activityCount", question.getActivityCount());
    }

    private static String prepareRelations(QuestionNode question) {
        return question.getTags().stream()
                .map(tag -> mapTagged(question.getQuestionId(), tag))
                .collect(Collectors.joining(" "));
    }

    public static String mapTagged(Long questionId, String tag) {
        return " MERGE (q" + questionId + ")" +
                "-[:Tagged]-" +
                "(t" + tag.replaceAll("[^a-zA-Z0-9]", "n_") + ") ";
    }

    public static String prepareActivityRelations(QuestionNode question) {
        return question.getUsers().values().stream()
                .map(u -> mapActivity(question.getQuestionId(), u))
                .collect(Collectors.joining(" "));
    }

    public static String prepareUserNode(UserNode user) {
        String label = "u" + user.getUserId();
        return " MERGE (" + label + ":User" + "{userId:" + user.getUserId() + "})"
                + " ON CREATE SET "
                + setStringProperty(label, "name", user.getDisplayName()) + ","
                + setProperty(label, "acceptRate", user.getAcceptRate()) + ","
                + setProperty(label, "reputation", user.getReputation())
                + " ON MATCH SET "
                + setProperty(label, "acceptRate", user.getAcceptRate()) + ","
                + setProperty(label, "reputation", user.getReputation());
    }

    public static String mapActivity(Long questionId, UserNode user) {
        String relLabel = "a" + user.getUserId() + "q" + questionId;
        return " MERGE (q" + questionId + ")" +
                "-[" + relLabel + ":Activity]-" +
                "(u" + user.getUserId() + ") ";
//                + " ON CREATE SET " + setProperty(relLabel, "score", user.getActivityScore())
//                + " ON MATCH SET " + setProperty(relLabel, "score",user.getActivityScore());
    }

    private static String setProperty(String label, String property, Object value) {
        return label + "." + property + " = " + value;
    }

    private static String setStringProperty(String label, String property, Object value) {
        return label + "." + property + " = \"" + value + "\"";
    }

}
