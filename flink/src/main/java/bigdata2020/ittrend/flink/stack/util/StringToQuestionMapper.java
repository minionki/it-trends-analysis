package bigdata2020.ittrend.flink.stack.util;

import bigdata2020.ittrend.flink.stack.model.json.QuestionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.api.common.functions.MapFunction;

import java.io.IOException;

public class StringToQuestionMapper implements MapFunction<String, QuestionResponse> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public QuestionResponse map(String json) {
        try {
            return mapper.readValue(json, QuestionResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
