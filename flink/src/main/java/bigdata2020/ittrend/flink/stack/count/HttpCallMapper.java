package bigdata2020.ittrend.flink.stack.count;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HttpCallMapper implements MapFunction<String, String> {

    private static final String URI = "http://10.100.1.154:8080/update"; //TO BE CHANGED

    @Override
    public String map(String s) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost request = prepareRequest(s);
            client.execute(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public HttpPost prepareRequest(String json) {
        HttpPost request = new HttpPost(URI);
        request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
        request.getEntity();
        return request;
    }

}
