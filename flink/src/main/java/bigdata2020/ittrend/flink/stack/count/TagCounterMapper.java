package bigdata2020.ittrend.flink.stack.count;

import bigdata2020.ittrend.flink.stack.model.count.TagCount;
import bigdata2020.ittrend.flink.stack.model.count.TagCountRequest;
import bigdata2020.ittrend.flink.stack.model.json.AnswerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.api.common.functions.MapFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TagCounterMapper implements MapFunction<AnswerResponse, String> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String map(AnswerResponse answerResponse) throws JsonProcessingException {

        try {
            Map<String, TagCount> tags = new HashMap<>();

            answerResponse.getItems().stream()
                    .flatMap(a -> a.getTags().stream())
                    .forEach(tag -> {
                        TagCount tagCount = tags.get(tag);
                        if (tagCount == null) {
                            tagCount = TagCount.builder().name(tag).build();
                        }
                        tagCount.setCount(tagCount.getCount() + 1);
                        tags.put(tag, tagCount);
                    });

            TagCountRequest request = TagCountRequest.builder().tags(tags.values()).build();

            return mapper.writeValueAsString(request);
        } catch(Exception e) {
            return mapper.writeValueAsString(TagCountRequest.builder().tags(new ArrayList<>()).build());
        }
    }
}
