package bigdata2020.ittrend.flink;

import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacketBuilder;
import org.apache.flink.streaming.connectors.nifi.NiFiSink;
import org.apache.flink.streaming.connectors.nifi.NiFiSource;
import org.apache.nifi.remote.client.SiteToSiteClient;
import org.apache.nifi.remote.client.SiteToSiteClientConfig;
import org.apache.nifi.remote.protocol.SiteToSiteTransportProtocol;

import java.util.concurrent.TimeUnit;

import static org.apache.nifi.remote.protocol.SiteToSiteTransportProtocol.HTTP;

public class EnvironmentFactory {
    private static final String NIFI_URL = "http://10.100.4.195:8080/nifi/";
    private static final SiteToSiteTransportProtocol TRANSPORT_PROTOCOL = HTTP;


    public static StreamExecutionEnvironment defaultStreamEnvironment() {
        final StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        environment.setRestartStrategy(RestartStrategies.fixedDelayRestart(10, Time.of(300, TimeUnit.SECONDS)));
        return environment;
    }

    public static NiFiSource nifiSource(String sourcePort) {
        return new NiFiSource(s2sConfig(sourcePort));
    }

    public static <T> NiFiSink<T> niFiSink(String sinkPort, NiFiDataPacketBuilder<T> packetBuilder) {
        return new NiFiSink<>(s2sConfig(sinkPort), packetBuilder);
    }

    public static SiteToSiteClientConfig s2sConfig(String portName) {
        return new SiteToSiteClient.Builder()
                .url(NIFI_URL)
                .portName(portName)
                .transportProtocol(TRANSPORT_PROTOCOL)
                .buildConfig();
    }
}
