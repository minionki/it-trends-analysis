package bigdata2020.ittrend.flink;

import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacket;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacketBuilder;
import org.apache.flink.streaming.connectors.nifi.NiFiSink;
import org.apache.flink.streaming.connectors.nifi.NiFiSource;
import org.apache.nifi.remote.client.SiteToSiteClient;
import org.apache.nifi.remote.client.SiteToSiteClientConfig;
import org.apache.nifi.remote.protocol.SiteToSiteTransportProtocol;

import java.util.concurrent.TimeUnit;

import static org.apache.nifi.remote.protocol.SiteToSiteTransportProtocol.HTTP;


/**
 * Skeleton for a Flink Streaming Job.
 *
 * <p>For a tutorial how to write a Flink streaming application, check the
 * tutorials and examples on the <a href="https://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution, run
 * 'mvn clean package' on the command line.
 *
 * <p>If you change the name of the main class (with the public static void main(String[] args))
 * method, change the respective entry in the POM.xml file (simply search for 'mainClass').
 */
public class StreamingJob {

	private static final String NIFI_URL = "http://10.100.4.195:8080/nifi/";
	private static final String SOURCE_PORT_NAME = "FlinkSource";
	private static final String SINK_PORT_NAME = "FlinkSink";
	private static final SiteToSiteTransportProtocol TRANSPORT_PROTOCOL = HTTP;

	public static SiteToSiteClientConfig sourceS2sConfig() {
		return s2sConfig(SOURCE_PORT_NAME);
	}

	public static SiteToSiteClientConfig sinkS2sConfig() {
		return s2sConfig(SINK_PORT_NAME);
	}

	public static SiteToSiteClientConfig s2sConfig(String portName) {
		return new SiteToSiteClient.Builder()
				.url(NIFI_URL)
				.portName(portName)
				.transportProtocol(TRANSPORT_PROTOCOL)
				.buildConfig();
	}

	public static void main(String[] args) throws Exception {
		// set up the streaming execution environment
		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setRestartStrategy(RestartStrategies.fixedDelayRestart(10, Time.of(300, TimeUnit.SECONDS)));

		SourceFunction<NiFiDataPacket> nifiSource = new NiFiSource(sourceS2sConfig());
		SinkFunction<NiFiDataPacket> nifiSink = new NiFiSink<>(sinkS2sConfig(), (NiFiDataPacketBuilder<NiFiDataPacket>) (niFiDataPacket, runtimeContext) -> niFiDataPacket);

		env.addSource(nifiSource).addSink(nifiSink);

		// execute program
		env.execute("Flink Sample");
	}

}
