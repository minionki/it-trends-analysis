package bigdata2020.ittrend.flink.git.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TopicNode {

    private String tag;
    private long gitCount = 0L;
    private Map<String, TopicRelation> connected;
}
