package bigdata2020.ittrend.flink.git.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContributorNode {

    private String displayName;

    private int contributions;

    private Map<String, TopicUserRelation> tags;

}
