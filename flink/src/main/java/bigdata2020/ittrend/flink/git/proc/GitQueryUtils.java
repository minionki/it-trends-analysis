package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.neo4j.ContributorNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicRelation;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicUserRelation;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collection;
import java.util.stream.Collectors;

public class GitQueryUtils {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
    }

    public static String mergeTagList(Collection<TopicNode> tagList) {
        return tagList.stream()
                .map(GitQueryUtils::prepareTopicNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeUserList(Collection<ContributorNode> userList) {
        return userList.stream()
                .map(GitQueryUtils::prepareContributorNode)
                .collect(Collectors.joining(" "));
    }

    public static String mergeTagRelations(Collection<TopicNode> tagList) {
        return tagList.stream()
                .flatMap(tag -> tag.getConnected().values().stream())
                .map(GitQueryUtils::prepareTopicRelationship)
                .collect(Collectors.joining(" "));
    }

    public static String mergeUserTagRelations(Collection<ContributorNode> userList) {
        return userList.stream()
                .flatMap(user -> user.getTags().values().stream())
                .map(GitQueryUtils::prepareContributorTagRelation)
                .collect(Collectors.joining(" "));
    }

    public static String matchTags(Collection<TopicNode> tagList) {
        return tagList.stream()
                .map(TopicNode::getTag)
                .collect(Collectors.joining("','", "MATCH (tag:Tag) where tag.tag in ['", "']"));
    }

    public static String matchTags2(Collection<TopicNode> tagList) {
        return tagList.stream()
                .map(TopicNode::getTag)
                .collect(Collectors.joining("','", "MATCH (tag2:Tag) where tag.tag in ['", "']"));
    }

    public static String matchUsers(Collection<ContributorNode> userList) {
        return userList.stream()
                .map(ContributorNode::getDisplayName)
                .collect(Collectors.joining("','", "MATCH (user:User) where user.name in ['", "']"));
    }

    public static String createConnections(Collection<TopicNode> tagList, Collection<ContributorNode> userList) {
        return matchTags(tagList) + " " + matchTags2(tagList) + " " + matchUsers(userList) + " " +
                " MERGE (tag)-[connected:Connected]-(tag2)  ON CREATE SET connected.gitCount = 1 ON MATCH SET connected.gitCount = connected.gitCount+1" +
                " MERGE (user)-[knows:Knows]-(tag)  ON CREATE SET knows.gitCount = 1 ON MATCH SET knows.gitCount = knows.gitCount+1";

    }

    private static String prepareTopicNode(TopicNode tag) {
        String label = tagLabel(tag.getTag());

        return " MERGE (" + label + ":Tag" + "{tag:\'" + tag.getTag() + "\'})"
                + " ON CREATE SET "
                + setProperty(label, "gitCount", tag.getGitCount())
                + " ON MATCH SET "
                + addToProperty(label, "gitCount", tag.getGitCount());
    }

    private static String prepareTopicRelationship(TopicRelation tagRelation) {
        String fromTag = tagLabel(tagRelation.getFrom());
        String toTag = tagLabel(tagRelation.getTo());
        String relLabel = fromTag + toTag;

        return " MERGE (" + fromTag + ")" +
                "-[" + relLabel + ":Connected]-" +
                "(" + toTag + ") "
                + " ON CREATE SET " + setProperty(relLabel, "gitCount", tagRelation.getGitCount())
                + " ON MATCH SET " + addToProperty(relLabel, "gitCount", tagRelation.getGitCount());
    }

    public static String prepareContributorTagRelation(TopicUserRelation userTagRelation) {
        String tagLabel = tagLabel(userTagRelation.getTag());
        String userLabel = userLabel(userTagRelation.getUserLogin());
        String relLabel = tagLabel + userLabel;
        return " MERGE (" + tagLabel + ")" +
                "-[" + relLabel + ":Knows]-" +
                "(" + userLabel + ") "
                + " ON CREATE SET " + setProperty(relLabel, "gitCount", userTagRelation.getGitCount())
                + " ON MATCH SET " + addToProperty(relLabel, "gitCount", userTagRelation.getGitCount());
    }

    public static String prepareContributorNode(ContributorNode user) {
        String label = "u" + userLabel(user.getDisplayName());
        return " MERGE (" + label + ":User" + "{name:'" + user.getDisplayName() + "'})"
                + " ON CREATE SET "
                + setStringProperty(label, "name", user.getDisplayName()) + ","
                + setProperty(label, "contributions", user.getContributions())
                + " ON MATCH SET "
                + addToProperty(label, "contributions", user.getContributions());
    }

    private static String tagLabel(String tag) {
        return "t" + tag.replaceAll("[^a-zA-Z0-9]", "n_");
    }

    private static String userLabel(String login) {
        return "u" + login.replaceAll("[^a-zA-Z0-9]", "n_");
    }

    private static String setProperty(String label, String property, Object value) {
        return label + "." + property + " = " + value;
    }

    private static String addToProperty(String label, String property, Object value) {
        return label + "." + property + " = " + label + "." + property + "+" + value;
    }


    private static String setStringProperty(String label, String property, Object value) {
        return label + "." + property + " = \"" + value + "\"";
    }
}
