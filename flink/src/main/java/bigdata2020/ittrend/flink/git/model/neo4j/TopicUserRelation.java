package bigdata2020.ittrend.flink.git.model.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TopicUserRelation {
    private String userLogin;
    private String tag;
    private long gitCount = 0L;
}
