package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.CleanRepo;
import bigdata2020.ittrend.flink.git.model.Neo4jBatch;
import bigdata2020.ittrend.flink.git.model.json.Contributor;
import bigdata2020.ittrend.flink.git.model.neo4j.ContributorNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicRelation;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicUserRelation;
import org.apache.flink.api.common.functions.MapFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GitGraphMapper implements MapFunction<CleanRepo, Neo4jBatch> {

    @Override
    public Neo4jBatch map(CleanRepo cleanRepo) {

        List<TopicNode> topics = prepareTopicNodes(cleanRepo);
        List<ContributorNode> contributors = prepareContributorNodes(cleanRepo);

        return Neo4jBatch.builder()
                .topics(topics)
                .contributors(contributors)
                .build();
    }


    private List<ContributorNode> prepareContributorNodes(CleanRepo cleanRepo) {

        if (cleanRepo.getContributors() == null) {
            cleanRepo.setContributors(new ArrayList<>());
        }

        if (cleanRepo.getTopics() == null) {
            cleanRepo.setTopics(new ArrayList<>());
        }

        return cleanRepo.getContributors().stream()
                .map(c -> ContributorNode
                        .builder()
                        .contributions(c.getContributions())
                        .displayName(c.getLogin())
                        .tags(topicUserRelations(c, cleanRepo.getTopics()))
                        .build()
                )
                .collect(Collectors.toList());
    }

    private Map<String, TopicUserRelation> topicUserRelations(Contributor c, List<String> topics) {
        Map<String, TopicUserRelation> relations = new HashMap<>();

        for (String topic : topics) {
            relations.put(topic, TopicUserRelation.builder().gitCount(1).tag(topic).userLogin(c.getLogin()).build());
        }

        return relations;
    }

    private List<TopicNode> prepareTopicNodes(CleanRepo cleanRepo) {
        List<String> topics = cleanRepo.getTopics();
        List<TopicNode> result = new ArrayList<>();

        for (int from = 0; from < topics.size(); from++) {

            String fromTopic = topics.get(from);
            Map<String, TopicRelation> relations = new HashMap<>();

            for (int to = from + 1; to < topics.size(); to++) {
                String toTopic = topics.get(to);
                relations.put(toTopic, TopicRelation.builder().from(fromTopic).to(toTopic).gitCount(1).build());
            }

            result.add(TopicNode
                    .builder()
                    .gitCount(1)
                    .tag(fromTopic)
                    .connected(relations)
                    .build()
            );
        }

        return result;
    }
}
