package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.Neo4jBatch;
import bigdata2020.ittrend.flink.git.model.neo4j.ContributorNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicNode;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GitQueryMapper implements FlatMapFunction<Neo4jBatch, String> {

    private static final int QUERY_SIZE_LIMIT = 64 * 1024;

    @Override
    public void flatMap(Neo4jBatch neo4jBatch, Collector<String> collector) {
        collector.collect(nodeQuery(neo4jBatch));

        if (neo4jBatch.getTopics().size() > 0) {
            collector.collect(relationQuery(neo4jBatch));
        }
    }

    public List<String> toList(Neo4jBatch neo4jBatch) {
        List<String> list = new ArrayList<>();
        list.add(nodeQuery(neo4jBatch));
        if (neo4jBatch.getTopics().size() > 0) {
            list.add(relationQuery(neo4jBatch));
        }
        return list;
    }

    private List<String> splitterQuery(List<TopicNode> topics, List<ContributorNode> contributors) {
        String query = limitedTopics(topics, contributors);

        if (query.length() > QUERY_SIZE_LIMIT) {
            List<String> left = splitterQuery(topics, firstHalf(contributors));
            List<String> right = splitterQuery(topics, secondHalf(contributors));
            left.addAll(right);
            return left;
        }
        else {
            List<String> list = new ArrayList<>();
            list.add(query);
            return list;
        }
    }

    private <T> List<T> firstHalf(List<T> items) {
        int half = items.size() / 2;
        return items.stream().limit(half).collect(Collectors.toList());
    }

    private <T> List<T> secondHalf(List<T> items) {
        int half = items.size() / 2;
        return items.stream().skip(half).collect(Collectors.toList());
    }


    private String nodeQuery(Neo4jBatch neo4jBatch) {
        return GitQueryUtils.mergeTagList(neo4jBatch.getTopics())
                + " "
                + GitQueryUtils.mergeUserList(neo4jBatch.getContributors());
    }

    private String relationQuery(Neo4jBatch neo4jBatch) {
        return GitQueryUtils.createConnections(neo4jBatch.getTopics(), neo4jBatch.getContributors());
    }

    private String normalQuery(Neo4jBatch neo4jBatch) {
        return GitQueryUtils.mergeTagList(neo4jBatch.getTopics())
                + " "
                + GitQueryUtils.mergeUserList(neo4jBatch.getContributors())
                + " "
                + GitQueryUtils.mergeTagRelations(neo4jBatch.getTopics())
                + " "
                + GitQueryUtils.mergeUserTagRelations(neo4jBatch.getContributors());
    }

    private String limitedTopics(List<TopicNode> topics, List<ContributorNode> contributors) {
        return GitQueryUtils.mergeTagList(topics)
                + " "
                + GitQueryUtils.mergeUserList(contributors)
                + " "
                + GitQueryUtils.mergeTagRelations(topics)
                + " "
                + GitQueryUtils.mergeUserTagRelations(contributors);
    }
}
