package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.json.GitData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.api.common.functions.MapFunction;

import java.io.IOException;

public class ParseGitDataJsonMapper implements MapFunction<String, GitData> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public GitData map(String content) {
        try {
            return mapper.readValue(content, GitData.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
