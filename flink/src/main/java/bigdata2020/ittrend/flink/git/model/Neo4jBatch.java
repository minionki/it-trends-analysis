package bigdata2020.ittrend.flink.git.model;

import bigdata2020.ittrend.flink.git.model.neo4j.ContributorNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Neo4jBatch {
    private List<TopicNode> topics;
    private List<ContributorNode> contributors;
}
