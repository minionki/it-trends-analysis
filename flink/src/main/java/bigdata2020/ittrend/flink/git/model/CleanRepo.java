package bigdata2020.ittrend.flink.git.model;

import bigdata2020.ittrend.flink.git.model.json.Contributor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CleanRepo {

    private String repoName;

    private List<String> topics;

    private List<Contributor> contributors;
}
