package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.CleanRepo;
import bigdata2020.ittrend.flink.git.model.json.Contributor;
import bigdata2020.ittrend.flink.git.model.json.GitData;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JoinRepoMapper implements FlatMapFunction<GitData, CleanRepo> {

    @Override
    public void flatMap(GitData gitData, Collector<CleanRepo> collector) {
        final Map<String, List<Contributor>> contributors = gitData.getContributors().stream()
                .collect(Collectors.groupingBy(Contributor::getRepoName));

        try {
            gitData.getRepos().stream()
                    .map(repo -> CleanRepo.builder()
                            .repoName(repo.getRepoName())
                            .topics(repo.getTopics().getNames())
                            .contributors(contributors.get(repo.getRepoName()))
                            .build()
                    )
                    .forEach(collector::collect);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CleanRepo> toList(GitData gitData) {
        final Map<String, List<Contributor>> contributors = gitData.getContributors().stream()
                .collect(Collectors.groupingBy(Contributor::getRepoName));

        return gitData.getRepos().stream()
                .map(repo -> CleanRepo.builder()
                        .repoName(repo.getRepoName())
                        .topics(repo.getTopics().getNames())
                        .contributors(contributors.get(repo.getRepoName()))
                        .build()
                )
                .collect(Collectors.toList());
    }
}
