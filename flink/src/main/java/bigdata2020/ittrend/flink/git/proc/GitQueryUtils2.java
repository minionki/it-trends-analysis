package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.neo4j.ContributorNode;
import bigdata2020.ittrend.flink.git.model.neo4j.TopicNode;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class GitQueryUtils2 {


    public static String prepareTopicNode(TopicNode tag) {
        String label = tagLabel(tag.getTag());

        return " MERGE (" + label + ":Tag" + "{tag:\'" + tag.getTag() + "\'})"
                + " ON CREATE SET "
                + setProperty(label, "gitCount", tag.getGitCount())
                + " ON MATCH SET "
                + setProperty(label, "gitCount", tag.getGitCount());
    }

    public static String prepareContributorNode(ContributorNode user) {
        String label = "u" + userLabel(user.getDisplayName());
        return " MERGE (" + label + ":User" + "{name:'" + user.getDisplayName() + "'})"
                + " ON CREATE SET "
                + setStringProperty(label, "name", user.getDisplayName()) + ","
                + setProperty(label, "contributions", user.getContributions())
                + " ON MATCH SET "
                + setProperty(label, "contributions", user.getContributions());
    }

    public static String matchFrom(String tag) {
        return "MATCH (from:Tag{tag:\'" + tag + "\'})";
    }

    public static String matchTo(Collection<String> tagList) {
        return tagList.stream().collect(Collectors.joining("','", "MATCH (to:Tag) where to.tag in ['", "']"));
    }

    public static String connected(Long count) {
        return "MERGE (from)-[connected:Connected]-(to) ON CREATE SET connected.gitCount = " + count + " ON MATCH SET connected.gitCount = " + count;
    }

    public static String matchUser(String name) {
        return "MATCH (user:User{name:\'" + name + "\'})";
    }

    public static String matchTag(Collection<String> tagList) {
        return tagList.stream().collect(Collectors.joining("','", "MATCH (tag:Tag) where tag.tag in ['", "']"));
    }

    public static String knows(Long count) {
        return "MERGE (user)-[knows:Knows]-(tag) ON CREATE SET knows.gitCount = " + count + " ON MATCH SET knows.gitCount = " + count;
    }

    private static String tagLabel(String tag) {
        return "t" + tag.replaceAll("[^a-zA-Z0-9]", "n_");
    }

    private static String userLabel(String login) {
        return "u" + login.replaceAll("[^a-zA-Z0-9]", "n_");
    }

    private static String setProperty(String label, String property, Object value) {
        return label + "." + property + " = " + value;
    }

    private static String addToProperty(String label, String property, Object value) {
        return label + "." + property + " = " + label + "." + property + "+" + value;
    }


    private static String setStringProperty(String label, String property, Object value) {
        return label + "." + property + " = \"" + value + "\"";
    }
}
