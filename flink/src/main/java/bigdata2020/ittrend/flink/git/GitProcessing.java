package bigdata2020.ittrend.flink.git;

import bigdata2020.ittrend.flink.EnvironmentFactory;
import bigdata2020.ittrend.flink.common.StringNifiPacketBuilder;
import bigdata2020.ittrend.flink.git.proc.GitGraphMapper;
import bigdata2020.ittrend.flink.git.proc.GitQueryMapper;
import bigdata2020.ittrend.flink.git.proc.JoinRepoMapper;
import bigdata2020.ittrend.flink.git.proc.ParseGitDataMapper;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacket;

public class GitProcessing {

    private static final String SOURCE_PORT_NAME = "GitForFlink";
    private static final String SINK_PORT_NAME = "Neo4jQueryBackup";

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment environment = EnvironmentFactory.defaultStreamEnvironment();

        SourceFunction<NiFiDataPacket> nifiSource = EnvironmentFactory.nifiSource(SOURCE_PORT_NAME);
        SinkFunction<String> nifiSink = EnvironmentFactory.niFiSink(SINK_PORT_NAME, new StringNifiPacketBuilder());

        environment
                .addSource(nifiSource)
                .map(new ParseGitDataMapper())
                .flatMap(new JoinRepoMapper())
                .map(new GitGraphMapper())
                .flatMap(new GitQueryMapper())
                .addSink(nifiSink);

        // execute program
        environment.execute("Flink Sample");
    }
}
