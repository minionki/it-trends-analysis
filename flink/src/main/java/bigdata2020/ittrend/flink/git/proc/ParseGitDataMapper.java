package bigdata2020.ittrend.flink.git.proc;

import bigdata2020.ittrend.flink.git.model.json.GitData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacket;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ParseGitDataMapper implements MapFunction<NiFiDataPacket, GitData> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public GitData map(NiFiDataPacket niFiDataPacket) {
        String content = new String(niFiDataPacket.getContent(), StandardCharsets.UTF_8);
        try {
            return mapper.readValue(content, GitData.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

