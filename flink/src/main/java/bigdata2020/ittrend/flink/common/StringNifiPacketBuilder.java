package bigdata2020.ittrend.flink.common;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.configuration.ConfigConstants;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacket;
import org.apache.flink.streaming.connectors.nifi.NiFiDataPacketBuilder;
import org.apache.flink.streaming.connectors.nifi.StandardNiFiDataPacket;

import java.util.HashMap;

public class StringNifiPacketBuilder implements NiFiDataPacketBuilder<String> {

    @Override
    public NiFiDataPacket createNiFiDataPacket(String s, RuntimeContext runtimeContext) {
        return new StandardNiFiDataPacket(s.getBytes(ConfigConstants.DEFAULT_CHARSET), new HashMap<>());
    }
}
